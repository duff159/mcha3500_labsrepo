* commit 3e9717ff42a0f3343c8808debbebe6dfbe3411cc
| Author: Ryan Duffield <ryan.duffield@uon.edu.au>
| Date:   Sun Nov 15 11:53:21 2020 +1100
| 
|     Quick update
| 
* commit 3090b7e86e4157421a9dda5dd60b736f00cfcfd5
| Author: Ryan Duffield <ryan.duffield@uon.edu.au>
| Date:   Wed Nov 11 12:14:44 2020 +1100
| 
|     Add turning/spin serial commands
| 
* commit 3ec1ea3a0161643a52b810b4c73daf78d709ba60
| Author: Ryan Duffield <ryan.duffield@uon.edu.au>
| Date:   Wed Nov 11 11:29:31 2020 +1100
| 
|     Final Tuning
| 
* commit 7abf4d59f5eed33999728cecae4327e35bd227a3
| Author: Ryan Duffield <ryan.duffield@uon.edu.au>
| Date:   Tue Nov 10 17:20:07 2020 +1100
| 
|     More tuning
| 
* commit 6886f2b8f416e74b9bd2e13be8bb96973228e4aa
| Author: Ryan Duffield <ryan.duffield@uon.edu.au>
| Date:   Tue Nov 10 12:19:00 2020 +1100
| 
|     Controller tuning
| 
* commit af10b0598ac31f4cb0af11037380b8917ba4f4ba
| Author: Ryan Duffield <ryan.duffield@uon.edu.au>
| Date:   Tue Nov 10 10:11:43 2020 +1100
| 
|     Quick update
| 
* commit ab9475f2d2a6ae83d57052550bc1f2464d863743
| Author: Ryan Duffield <ryan.duffield@uon.edu.au>
| Date:   Mon Nov 9 18:31:52 2020 +1100
| 
|     Recalibrate observer and tuning (ROBOT BALANCING!!)
| 
* commit 9ac130120f76242419f22d65526d55f920079fd6
| Author: Ryan Duffield <ryan.duffield@uon.edu.au>
| Date:   Sat Nov 7 21:37:06 2020 +1100
| 
|     More tuning
| 
* commit 060b52082d1056f9902ca5a47595e415da135ffc
| Author: Ryan Duffield <ryan.duffield@uon.edu.au>
| Date:   Sat Nov 7 11:37:33 2020 +1100
| 
|     Button working
| 
* commit aa74672195ac4b9a421ce7526d451154da8b07d3
| Author: Ryan Duffield <ryan.duffield@uon.edu.au>
| Date:   Sat Nov 7 11:25:08 2020 +1100
| 
|     Add NUCLEO Button to start controller
| 
* commit b136fb42059c1a832ffc9286f23371ed5abf13c2
| Author: Ryan Duffield <ryan.duffield@uon.edu.au>
| Date:   Fri Nov 6 22:28:30 2020 +1100
| 
|     Add PIL slew rate ctrl test/Tuning
| 
* commit 057ac29533b9c2a341a845089eeb5ed2ed7dd5f7
| Author: Ryan Duffield <ryan.duffield@uon.edu.au>
| Date:   Thu Nov 5 21:48:11 2020 +1100
| 
|     Controller tuning
| 
* commit 0201a3ad4ef334471b106a70be139c576626b3b3
| Author: Ryan Duffield <ryan.duffield@uon.edu.au>
| Date:   Wed Nov 4 20:38:46 2020 +1100
| 
|     Add new controller (with slew rate limit)
| 
* commit 2c02b2cf3dd5d1cb682d583d4527a5cce9cd69c0
| Author: Ryan Duffield <ryan.duffield@uon.edu.au>
| Date:   Wed Nov 4 19:20:24 2020 +1100
| 
|     Tuning
| 
* commit b3a44a55d3feb77a7aea9bfcf82a5e62b1df53e4
| Author: Ryan Duffield <ryan.duffield@uon.edu.au>
| Date:   Tue Nov 3 20:28:30 2020 +1100
| 
|     Retuned PI velocity control gains/add ctrl action saturation
| 
* commit c51144064cf538f3f8480d2d40a68549a4b224d0
| Author: Ryan Duffield <ryan.duffield@uon.edu.au>
| Date:   Fri Oct 30 20:45:28 2020 +1100
| 
|     Quick update (control action increasing to inf)
| 
* commit 8230707c31e58e8889b0d113eabe8f70f6dd19b7
| Author: Ryan Duffield <ryan.duffield@uon.edu.au>
| Date:   Fri Oct 30 17:50:49 2020 +1100
| 
|     Change observer back to 100Hz
| 
* commit 27bbfe6bf204ad3cb2cf897c9e78ae760fa35737
| Author: Ryan Duffield <ryan.duffield@uon.edu.au>
| Date:   Fri Oct 30 09:27:26 2020 +1100
| 
|     Tested sinusoid velocity tracking of motors (working)
| 
* commit 42f0e6a08bb7d61e4c09b47a3f9e1e01eb9d6bf7
| Author: Ryan Duffield <ryan.duffield@uon.edu.au>
| Date:   Thu Oct 29 13:40:25 2020 +1100
| 
|     Quick update (still not balancing)
| 
* commit f9784b822e4b6d2fbc5fad9dd338198d4efd403c
| Author: Ryan Duffield <ryan.duffield@uon.edu.au>
| Date:   Thu Oct 29 11:13:53 2020 +1100
| 
|     Add balancing control loop (not working)
| 
* commit 1c5299e13fe9f30a499944c3464f4292164e73fa
| Author: Ryan Duffield <ryan.duffield@uon.edu.au>
| Date:   Thu Oct 29 08:50:03 2020 +1100
| 
|     Changed signs of IMU outputs
| 
* commit e5c3c184e4cc355188797884afccbe46107df0f8
| Author: Ryan Duffield <ryan.duffield@uon.edu.au>
| Date:   Wed Oct 28 20:08:28 2020 +1100
| 
|     Added linear observer for IMU data estimation (working)
| 
* commit 204f5a115e9efa107bec001ff1f2dbeddbdd042b
| Author: Ryan Duffield <ryan.duffield@uon.edu.au>
| Date:   Wed Oct 28 18:46:51 2020 +1100
| 
|     Add velocity control to left motor
| 
* commit 2fed7d5597ba14b42a8962f9440fe81ce9b63370
| Author: Ryan Duffield <ryan.duffield@uon.edu.au>
| Date:   Wed Oct 28 17:21:28 2020 +1100
| 
|     Quick update
| 
* commit 22d59a400ac85f871a53615a8cfc64bc2a16800e
| Author: Ryan Duffield <ryan.duffield@uon.edu.au>
| Date:   Wed Oct 28 16:54:13 2020 +1100
| 
|     Set up observer matrix operations
| 
* commit 7865c4577bb65c0111f37f4f8719e30aac91740e
| Author: Ryan Duffield <ryan.duffield@uon.edu.au>
| Date:   Wed Oct 28 16:10:52 2020 +1100
| 
|     Add observer matrices
| 
* commit 010107b612b139ae68e6f51c5dd578f042a22ac4
| Author: Ryan Duffield <ryan.duffield@uon.edu.au>
| Date:   Wed Oct 28 11:18:54 2020 +1100
| 
|     Quick Update
| 
* commit 3a52dd5876b8f89d7fb563c5a72eb7e46b48da49
| Author: Ryan Duffield <ryan.duffield@uon.edu.au>
| Date:   Wed Oct 28 10:24:17 2020 +1100
| 
|     Add swing test logging function
| 
* commit 66b394046348d2b57226512cabb5e86b99028957
| Author: Ryan Duffield <ryan.duffield@uon.edu.au>
| Date:   Tue Oct 27 18:09:44 2020 +1100
| 
|     Removed feedforward from controller.c for velocity control
| 
* commit f36f1f3b22a4d1a798fa8835a9af5941a1d35050
| Author: Ryan Duffield <ryan.duffield@uon.edu.au>
| Date:   Tue Oct 27 14:53:12 2020 +1100
| 
|     Add basic velocity control loop (working)
| 
* commit 642db91be6624d5dfeac054419b1e8cd26a55664
| Author: Ryan Duffield <ryan.duffield@uon.edu.au>
| Date:   Tue Oct 27 13:16:00 2020 +1100
| 
|     Current control Update (regulation not great)
| 
* commit 36acc19672dfe68e53b45e3cfc233c4a36f3f5cf
| Author: Ryan Duffield <ryan.duffield@uon.edu.au>
| Date:   Mon Oct 26 16:05:16 2020 +1100
| 
|     Current Control update (working)
| 
* commit 9a1494ff2182b8aaa4a2b95c86d089dca7b79ca4
| Author: Ryan Duffield <ryan.duffield@uon.edu.au>
| Date:   Sun Oct 25 12:08:42 2020 +1100
| 
|     Current control update 25th oct
| 
* commit e99530ce09132c1171f82f0d58c2951f48d2183c
| Author: Ryan Duffield <ryan.duffield@uon.edu.au>
| Date:   Sat Oct 24 22:38:35 2020 +1100
| 
|     Current control update
| 
* commit 1f1e602c534a8686dc571fcf7dc6bf5826c9c772
| Author: Ryan Duffield <ryan.duffield@uon.edu.au>
| Date:   Sat Oct 24 17:26:20 2020 +1100
| 
|     Current control update (AWAITING TESTING)
| 
* commit 18f751de70f374f89b56a847a3663352a4c1f6b2
| Author: Ryan Duffield <ryan.duffield@uon.edu.au>
| Date:   Fri Oct 23 21:22:53 2020 +1100
| 
|     Current control incremental update
| 
* commit e1b3b4fb0253408fbd9c7ecd6018b4b4ed0f9529
| Author: Ryan Duffield <ryan.duffield@uon.edu.au>
| Date:   Thu Oct 22 22:46:00 2020 +1100
| 
|     Start of current control implenentation
| 
* commit dc40d266b9e6d52719e7d4741af770a3131421f7
| Author: Ryan Duffield <ryan.duffield@uon.edu.au>
| Date:   Thu Oct 22 10:03:54 2020 +1100
| 
|     Update frequency
| 
* commit 619c9bbf930ad1f85b44b8dc686290fb94f19264
| Author: Ryan Duffield <ryan.duffield@uon.edu.au>
| Date:   Wed Oct 21 18:20:05 2020 +1100
| 
|     Sin Voltage motor testing
| 
* commit b88184bd447d47b3d184d47b68da027b5416f314
| Author: Ryan Duffield <ryan.duffield@uon.edu.au>
| Date:   Tue Oct 20 21:03:15 2020 +1100
| 
|     Add reference feedforward
| 
* commit b5e19e46ae95cd191c5679a1616d89514836a79e
| Author: Ryan Duffield <ryan.duffield@uon.edu.au>
| Date:   Tue Oct 20 18:48:34 2020 +1100
| 
|     Improve control (didnt work)
| 
* commit e7a0a6434e3f3c681c7a40823edb81d5da6f7b14
| Author: Ryan Duffield <ryan.duffield@uon.edu.au>
| Date:   Mon Oct 19 22:29:05 2020 +1100
| 
|     Add motor voltage control
| 
* commit 8292f2a046e49f1dec6645b6eb38de06654dff22
| Author: Ryan Duffield <ryan.duffield@uon.edu.au>
| Date:   Mon Oct 19 15:58:48 2020 +1100
| 
|     Add current sense calibration
| 
* commit 9983ab612c253eb13fc2cd815bbbf2d2c268a5dc
| Author: Ryan Duffield <ryan.duffield@uon.edu.au>
| Date:   Sun Oct 18 16:08:57 2020 +1100
| 
|     Add Balancing Robot Controller Gains
| 
* commit d876f4ecf6c06dfe36ecb4c5fc74c8053a65e44e
| Author: Ryan Duffield <ryan.duffield@uon.edu.au>
| Date:   Sun Oct 18 11:21:49 2020 +1100
| 
|     Update
| 
* commit 6733e53f6ce7cae9695fcc9996b621d665428dfe
| Author: Ryan Duffield <ryan.duffield@uon.edu.au>
| Date:   Wed Oct 14 16:12:29 2020 +1100
| 
|     Current Sensing
| 
* commit 8927cc69a8185db7d09c9524754446ebd178b438
| Author: Ryan Duffield <ryan.duffield@uon.edu.au>
| Date:   Tue Oct 13 12:05:45 2020 +1100
| 
|     Add current logging (too slow)
| 
* commit 05acf00bed7f249b2109b46dcd434ead99acee9b
| Author: Ryan Duffield <ryan.duffield@uon.edu.au>
| Date:   Tue Oct 13 11:51:25 2020 +1100
| 
|     Update
| 
* commit 3e5eed0c579c543e7d644134cdc3f25fd8f266fc
| Author: Ryan Duffield <ryan.duffield@uon.edu.au>
| Date:   Tue Oct 13 11:24:41 2020 +1100
| 
|     Add BMS command to serial terminal
| 
* commit f74e0a78b6f135bc9029a544c030fa9b875c1b2f
| Author: Ryan Duffield <ryan.duffield@uon.edu.au>
| Date:   Mon Oct 12 20:00:14 2020 +1100
| 
|     Update
| 
* commit 6ec34e866e3ea298f05e86dae9c1dbe022a3f771
| Author: Ryan Duffield <ryan.duffield@uon.edu.au>
| Date:   Mon Oct 12 19:42:19 2020 +1100
| 
|     Added Encoder Logging to Left Motor
| 
* commit 86a1a4311a4a730771e1b8fb8b07146f026fdd5a
| Author: Ryan Duffield <ryan.duffield@uon.edu.au>
| Date:   Sun Oct 11 10:00:57 2020 +1100
| 
|     Update
| 
* commit ac2496376bddf0466f5ec6d5619a2b9a508277fb
| Author: Ryan Duffield <ryan.duffield@uon.edu.au>
| Date:   Tue Oct 6 21:46:12 2020 +1100
| 
|     Add motor terminal control
| 
* commit 1944d0cda4df06d87d710a6d3dda84b0dc586c61
| Author: Ryan Duffield <c3305704@uon.edu.au>
| Date:   Sun Sep 27 17:31:29 2020 +1000
| 
|     Initial Current Sense & BMS
| 
* commit 95915b72ee209ed1e65c3d7a96c76ded8b0ebfb7
| Author: Ryan Duffield <c3305704@uon.edu.au>
| Date:   Sun Sep 27 15:59:29 2020 +1000
| 
|     Initial Motor Control
| 
* commit 1f08bed5d0809220044b44ad14e98648a0cb6878
| Author: Ryan Duffield <c3305704@uon.edu.au>
| Date:   Wed Sep 23 16:49:59 2020 +1000
| 
|     Update
| 
* commit 0ecdc3654a6a34a1f2755d9937d5768f483bba06
| Author: Ryan Duffield <c3305704@uon.edu.au>
| Date:   Wed Sep 23 16:44:15 2020 +1000
| 
|     Completed Lab 8 Control
| 
* commit 2032187214722ea1a8ab779200ead95bc050e46d
| Author: Ryan Duffield <c3305704@uon.edu.au>
| Date:   Tue Sep 22 20:42:05 2020 +1000
| 
|     Added template code up to getControl
| 
* commit 26ac16799f4bc8bbc4d16b4dc503316b5a400b52
| Author: Ryan Duffield <c3305704@uon.edu.au>
| Date:   Tue Sep 22 20:26:19 2020 +1000
| 
|     Update before lab 8
| 
* commit 985c291f00f744756e0fee52457c70306bd5e50d
| Author: Ryan Duffield <c3305704@uon.edu.au>
| Date:   Wed Sep 16 15:36:05 2020 +1000
| 
|     Update
| 
* commit 6f46ecd3f316479ca2cd160f7dd63212e95b8ba5
| Author: Ryan Duffield <c3305704@uon.edu.au>
| Date:   Wed Sep 16 15:29:17 2020 +1000
| 
|     Add IMU data logging functionality
|   
*   commit 7824a4449c99bca4913c1a62a89272b4fd1e3fec
|\  Merge: 79329fa 844e24b
| | Author: Ryan Duffield <c3305704@uon.edu.au>
| | Date:   Wed Sep 16 12:25:12 2020 +1000
| | 
| |     Fetch IMU Files for Lab 7
| | 
| * commit 844e24bde59c14983316ee98c61a67511dfb25b1
| | Author: AlexF <alexander.fairclough@newcastle.edu.au>
| | Date:   Wed Sep 9 12:11:16 2020 +1000
| | 
| |     Add MPU6050 driver library
| | 
* | commit 79329fa3822c2c43477c10f6518cd19c92a5ac9e
| | Author: Ryan Duffield <c3305704@uon.edu.au>
| | Date:   Wed Sep 16 11:41:22 2020 +1000
| | 
| |     Add timer pointer functionality
| | 
* | commit 16b5ca289df0a4ae4d313a18d4278c2b5edf6402
| | Author: Ryan Duffield <c3305704@uon.edu.au>
| | Date:   Wed Sep 16 09:33:02 2020 +1000
| | 
| |     Update before Lab 7
| | 
* | commit 831a253943199b502764a433d1605463081f2301
| | Author: Ryan Duffield <c3305704@uon.edu.au>
| | Date:   Tue Sep 1 18:22:02 2020 +1000
| | 
| |     Add data logging to serial comms
| | 
* | commit 5f7ceab6776e2e37c45e4dc19ae1d553aaa3655a
| | Author: Ryan Duffield <c3305704@uon.edu.au>
| | Date:   Tue Sep 1 17:05:36 2020 +1000
| | 
| |     Add serial comm for printing Pot reading
| | 
* | commit 345209200801eef9f5e46efeee4ed09c7b5c8127
| | Author: Ryan Duffield <c3305704@uon.edu.au>
| | Date:   Wed Aug 19 16:17:36 2020 +1000
| | 
| |     Update
| | 
* | commit 29d31829ce0c9018178ee9bdd502d2735c35c99f
| | Author: Ryan Duffield <c3305704@uon.edu.au>
| | Date:   Wed Aug 19 15:44:22 2020 +1000
| | 
| |     Update motor file
| | 
* | commit f76508b201468b31371d352285655c55c4783020
| | Author: Ryan Duffield <c3305704@uon.edu.au>
| | Date:   Wed Aug 19 15:38:25 2020 +1000
| | 
| |     Add PWM output and encoder count for motor
| | 
* | commit 54d877dabd52cba39831e2ddb945698d6225c591
| | Author: Ryan Duffield <c3305704@uon.edu.au>
| | Date:   Wed Aug 19 14:30:50 2020 +1000
| | 
| |     Start of motor encoder function
| |   
* |   commit c7e77c8fb0a34ead251dff3ba8412f3ff2ce44d3
|\ \  Merge: 5ed2049 590470e
| | | Author: Ryan Duffield <c3305704@uon.edu.au>
| | | Date:   Wed Aug 19 14:17:42 2020 +1000
| | | 
| | |     Resolved conflict
| | | 
| * | commit 590470ea8ae14e8be33358885921e213004fdcc3
| | | Author: Ryan Duffield <c3305704@uon.edu.au>
| | | Date:   Wed Aug 19 14:10:14 2020 +1000
| | | 
| | |     Add PWM timer functionality for motor
| | | 
* | | commit 5ed2049593a6e20223b9c314c43dd01682e4d998
|/ /  Author: Ryan Duffield <c3305704@uon.edu.au>
| |   Date:   Wed Aug 19 12:00:29 2020 +1000
| |   
| |       Update before adding motor files
| | 
* | commit 7e7d527fc7f1fa301b7ffd5a2e48298e3fed8209
|/  Author: Ryan Duffield <c3305704@uon.edu.au>
|   Date:   Wed Aug 19 09:44:28 2020 +1000
|   
|       Add ADC read for pendulum
| 
* commit 3a0c32136ebca4865af3de4f59565be1a7ae8a78
| Author: Alex Fairclough <alexander.fairclough@newcastle.edu.au>
| Date:   Sun Aug 16 14:32:09 2020 +1000
| 
|     Change folder name from `robot` to `mcha3500labs`
| 
* commit 383f78580f49665336955c7c13c4c561df13c27d
| Author: Alex Fairclough <alexander.fairclough@newcastle.edu.au>
| Date:   Sun Aug 16 14:31:46 2020 +1000
| 
|     Fix makefile not generating 'bin' directory
| 
* commit d0ebd7979adee9230f179900a8b17294c5f2f802
  Author: AlexF <alexander.fairclough@newcastle.edu.au>
  Date:   Fri Aug 14 18:06:06 2020 +1000
  
      Initial Commit.
