#include <stddef.h>
#include <stdint.h>
#include <stdlib.h>
#include <math.h>
#include "cmsis_os2.h"
#include "uart.h"
#include "stm32f4xx_hal.h"
#include "arm_math.h" /* Include STM32 DSP matrix libraries */
#include "controller.h"
#include "observer.h"
#include "observer.h"
#include "velocityControl.h"
#include "motor.h"
#include "IMU.h"

#ifndef M_PI
#define M_PI 3.14159265358979323846
#endif
static float velRef_old = 0.0;
static float slew = 1.0;//1.0
/* Set up timer to update loop at set sample time */
/* Misc. varables */
static uint16_t timeCount;
static double freq = 200; // Hz
static double sampTime = 0;
static uint8_t _is_init = 0;
static uint8_t _is_running = 0;

/* Define variables */
static float P_theta = 0.0;  // Momentum of chassis
static float u = 0.0;        // Control input (dPhi)
static int i = 0; // Counter
static float rightTurnVal = 0.0;
static float leftTurnVal = 0.0;

/* Structures defined for timer */
osTimerId_t _controlLoop_timerID;
osTimerAttr_t _controlLoop_timerAttr = {
    .name = "controlLoopTimer"
};

static void setMomentum(float theta, float dtheta, float dphi)
{
  float Jw = 0.2;
  float Jc = 0.2;
  float mw = 0.054;
  float mc = 0.680; //0.726
  float r  = 0.04;
  float l  = 0.211; //0.2236;

  /* Calculate momentum of the chassis */
  P_theta = (Jw+mw*(r*r)+mc*(r*r)+mc*r*l*cos(theta))*dphi + (Jw+Jc+mw*(r*r)+mc*(r*r)+mc*(l*l)+2*mc*r*l*cos(theta))*dtheta;
}

void spin(float spinVal)
{
  leftTurnVal = -spinVal;
  rightTurnVal = spinVal;
}

void setLeftTurn(float turnVal)
{
  leftTurnVal = turnVal;
}

void setRightTurn(float turnVal)
{
  rightTurnVal = turnVal;
}

float getMomentum(void)
{
  return P_theta;
}

static void controlCalc_update(void)
{
  /* Calculate/update everything need for control loop */
  /* Update states */
              // Angle        // Velocity     // Ctrl input
  setMomentum(obsv_get_y1h(), obsv_get_y2h(), getControl());

  // P_theta (chassis momentum)
  ctrl_set_x1h(getMomentum());

  // theta (chassis angle)
  ctrl_set_x2h(obsv_get_y1h());

  // phi (wheel angle) // NOTE: don't need this state, x3 is now the integrator
  //ctrl_set_x3h(angle);

  /* Update control */
  ctrl_update();

}

void controlLoop_start(void)
{
  /* Reset the time counter */
  timeCount = 0;
  /* Start data timer at 200Hz */
  if(!_is_running && _is_init)
    {
        /* Start the timer to run at 200Hz */
        osTimerStart(_controlLoop_timerID,(1.0/freq)*1000);
        _is_running = 1;
    }
}

void controlLoop_stop(void)
{
    /* Stop control loop timer */
    /* Check if timer is running */
    if (osTimerIsRunning(_controlLoop_timerID)){
        _is_running = 1;
    }

    if(_is_running && _is_init)
    {
        /* Stop the timer */
        osTimerStop(_controlLoop_timerID);
        _is_running = 0;

        /* Set motor voltages to zero */
        motorLeft(0.0);
        motorRight(0.0);
    } else {
        printf("Control is not currently running.\n");
    }
}

static void controlLoop_update(void *arg)
{
  /* Supress compiler warnings for unused arguments */
  UNUSED(arg);

  /* Turn off Controller if fallen over */
  if ((obsv_get_y1h()<1.0)&& (obsv_get_y1h()>-1.0)){

    // Let observer converge before controlling
    if (timeCount<1000) {
      /* Recalculate IMU Estimates */
      observerCalc_update();
    } else {
      /* Recalculate Control every 2nd sample time (100Hz)*/
      if (i < 1) {
        i++;
      } else {
        /* Recalculate IMU Estimates */
        observerCalc_update();
        /* Recalculate control Action */
        controlCalc_update();
        i = 0;
      }
      // Get reference vo=elocity to send to motors
      u = getControl();

      if (u-velRef_old > slew) {
        u = velRef_old+slew;
      } else if (u-velRef_old < slew) {
        u = velRef_old-slew;
      }

      setLeftVelRef(u+leftTurnVal); // Left wheel w* (-6.0)
      setRightVelRef(u+rightTurnVal); // Right wheel w*  (+6.0)

      /* Update wheel velocity control every sample time */
      velocityLeftCtrl_update();
      velocityRightCtrl_update();

      velRef_old = u;
    }
  } else {
    /* Stop Controller */
    controlLoop_stop();
  }

  /* Print Results */
  /* Time, Momentum, Chassis Angle, Wheel Aceeleration, Control Input, Wheel Velocity */
  //printf("%f,%f,%f,%f,  %f\n",sampTime,ctrl_get_x2h(),ctrl_get_u(),getControl(),getRVelocity());


  /* Increment timer count */
  timeCount++;
  sampTime = timeCount*(1.0/freq);

    /* Stop control loop after 20 sec */
    //if (sampTime >= 1.0) {
    /* Stop control loop */
    //  controlLoop_stop();
    //}
}

void controlLoop_timer_init(void)
{
  /* Initialise timer for use with control loop */
  if (!_is_init) {
      /* Create timer for communication to serial port */
      _controlLoop_timerID = osTimerNew(controlLoop_update, osTimerPeriodic, NULL, &_controlLoop_timerAttr);
      _is_init = 1;
  }
}
