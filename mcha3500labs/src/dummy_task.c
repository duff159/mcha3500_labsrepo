#include "dummy_task.h"

#include <stdint.h>
#include "IMU.h"
#include "stm32f4xx_hal.h"
#include "cmsis_os2.h"
#include "uart.h"
#include "pendulum.h"
#include "motor.h"
#include "currentSense.h"
#include "BMS.h"
#include "controlLoop.h"
#include "button.h"

static int ctrl_is_running=0;
static void dummy_task_update(void *arg);
static osThreadId_t _dummyTaskThreadID;
static osThreadAttr_t _dummyTaskThreadAttr =
{
    .name = "heartbeat",
    .priority = osPriorityIdle,
    .stack_size = 128
};

static uint8_t _is_running = 0;
static uint8_t _is_init = 0;

void dummy_task_init(void)
{
    if (!_is_init)
    {
        // CMSIS-RTOS API v2 Timer Documentation: https://www.keil.com/pack/doc/CMSIS/RTOS2/html/group__CMSIS__RTOS__TimerMgmt.html
        _dummyTaskThreadID = osThreadNew(dummy_task_update, NULL, &_dummyTaskThreadAttr);   // Create the thread in the OS scheduler.
        // Note: The thread starts automatically when osThreadNew is called
        _is_running = 1;
        _is_init = 1;
    }
}

void dummy_task_start(void)
{
    if (!_is_running)
    {
        osThreadResume(_dummyTaskThreadID);
        _is_running = 1;
    }
}

void dummy_task_stop(void)
{
    if (_is_running)
    {
        osThreadSuspend(_dummyTaskThreadID);
        _is_running = 0;
    }
}

uint8_t dummy_task_is_running(void)
{
    return _is_running;
}

void dummy_task_update(void *arg)
{
    UNUSED(arg);
    while(1)
    {
        // Add print statements for motor and potentiometer
        //float voltage = pendulum_read_voltage();
        //printf("The potentiometer voltage is: %f\n", voltage);
        //currentSense_read();
        float BMSvoltage = BMS_readVoltage();
        //printf("The BMS voltage is: %f\n", BMSvoltage);
        if (BMSvoltage < 2.60) {
          HAL_GPIO_TogglePin(GPIOB, GPIO_PIN_5);
        //  printf("LOW BATTERY!");
        }

        /* Check if USER-DEFINED button was pressed to start control loop */
        if (button_get_pressed() && !ctrl_is_running)
        {
          controlLoop_start();
          button_clear_pressed();
          ctrl_is_running = 1;

        }
        /*if ((button_get_pressed()) && (ctrl_is_running)) {
          controlLoop_stop();
          button_clear_pressed();
          ctrl_is_running = 0;
        }*/


        //int32_t encoderCount = motor_encoder_getValue();
        //printf("The Encoder Count is: %li\n", encoderCount);

        //int32_t encoderCount2 = -motor_encoder2_getValue();
        //printf("The Encoder 2 Count is: %li\n", encoderCount2);
        //IMU_read();
        //float accel_Y = get_accY();
        //printf("The Y Acceleration is: %f\n", accel_Y);

        //float accel_Z = get_accZ();
        //printf("The Z Acceleration is: %f\n", accel_Z);

        //float gyro_X = get_gyroX();
        //printf("The Angular Velocity is: %f\n", gyro_X);

        //double accAngle = get_acc_angle();
        //printf("The IMU angle is: %f\n", accAngle);

        //printf("--------------------------------------\n\n");
        // Non-blocking delay to wait
        osDelay(500);
    }
}

void dummy_task_deinit(void)
{
    _is_init = 0;
    _is_running = 0;
}
