// currentControl.c
#include <stdint.h>
#include <stdlib.h>
#include <math.h>
#include "stm32f4xx_hal.h"
#include "cmsis_os2.h"
#include "uart.h"
#include "motor.h"
#include "currentSense.h"
#include "data_logging.h"

 /* Set up timer to update loop at set sample time */
 /* Misc. varables */
 uint16_t timeCount;
 double freq = 100; // 100Hz
 double sampTime = 0;
 static uint8_t _is_init = 0;
 static uint8_t _is_running = 0;

 /* Structures defined for dtimer */
 osTimerId_t _currentCtrl_timerID;
 osTimerAttr_t _currentCtrl_timerAttr = {
     .name = "currentCtrlTimer"
 };

/* Define Variables */
float currentRef = 0.0; // Current Reference
float x = 0.0;       // Current
float z = 0.0;       // Integrator state
float z_dot = 0.0;   // Integrator State Derivative
float IStar = 0.0;   // Current Reference
float Ra = 10.4571;  // Armature Resistance
float Kw = 0.0031;   // Motor constant
float wa = 0.0;      // Motor Angular Velocity
float u = 0.0;       // Control input
float error = 0.0;   // I - I*
float velocityR = 0.0; // Right wheel velocity
float voltage = 0.0;  // Motor voltage

int32_t encROld = 0;
int32_t encRNew = 0;

/* Define PI Gains */
static float Kp =2.5;
static float Ki = 0.5;

/* Function defintions */
void setIState(float I)
{
  x = I;
}
static void setState(void)
{
  /* Update current reading of right motor */
  currentSense_read();
  x = getCurrent1();

}

float getState(void)
{
  return x;
}

void setIRef(float current)
{
  IStar = current;
}
static float getCurrentRef(void)
{
  return IStar;
}

//static float getError(void)
//{
  /* z_dot = I-I* */
//  float err = getState() - getCurrentRef();
//  return err;
//}

static void setMotorVelocity(void)
{
  encRNew = motor_encoder2_getValue();
  velocityR = -((((encRNew-encROld)/(48.0*34.014))*360.0)/0.01)*(3.14159/180);
  encROld = encRNew;

  /* Update motor velocity */
  wa = velocityR;
  //wa = getMotorRVelocity();
}
static float getMotorVelocity(void)
{
  return wa;
}

static void setMotorVoltage(float ctrl_in)
{
    /* Vin = Ra*Istar + Kw*wa + u */
    voltage = Ra*getCurrentRef() + Kw*getMotorVelocity() + ctrl_in;

    /* Check if voltage is below motors rating */
    if ((voltage >= 6.0) || (voltage <= -6.0)) {
      /* Voltage exceeds rated voltage */
      //printf("Warning! Required voltage exceeds rated voltage!\n");
    } else {
      /* Send voltage to right motor */
      motorRight(voltage);
    //  motorLeft(voltage);
      //printf("Voltage Applied!\n");
    }
}

/* Update control output */
void currentCtrl_update(void)
{
  /* Compute Velocity */
  setMotorVelocity();
  /* read current */
  setState();
  /* Read current error */
  error = (getState() - getCurrentRef());
  // Increment integral state i-*i
  z = z + 0.01*error;

  // Compute control signal u = -Kp*z_dot -Ki*z
  u = -Kp*error -Ki*z;

  // Compute voltage to send to motor
  setMotorVoltage(u);
}

float getIControl(void)
{
  currentCtrl_update();
  return u;
}

void currentCtrl_start(void)
{
  /* Reset the time counter */
  timeCount = 0;
  /* Start data logging timer at 100Hz */
  if(!_is_running && _is_init)
    {
        /* Start the timer to run at 100Hz */
        osTimerStart(_currentCtrl_timerID,10); //(1.0/freq)*1000);
        _is_running = 1;
    }
}

void currentCtrl_stop(void)
{
    /* Stop current control timer */
    /* Check if timer is running */
    if (osTimerIsRunning(_currentCtrl_timerID)){
        _is_running = 1;
    }

    if(_is_running && _is_init)
    {
        /* Stop the timer */
        osTimerStop(_currentCtrl_timerID);
        _is_running = 0;
        printf("Current control OFF. \n");
    } else {
        printf("Current control is not currently running.\n");
    }
}

static void currentLoop_update(void *arg)
{
  /* Supress compiler warnings for unused arguments */
  UNUSED(arg);

  /* Update current reference */
  if (sampTime <= 6.0) {
    currentRef = 0.1*sampTime;
  } else if (sampTime>= 6.0) {
    currentRef = 0.1*6.0;
  }
  //currentRef = -0.4*sin(1*sampTime);
  setIRef(currentRef);

  /* Update motor velocity */
  //motorVelocity_logging_start();
  /* Update Control */
  currentCtrl_update();

  /* Print Results */
  /* Time, Velocity, I*, I, Voltage */
  printf("%f,%f,%f,%f,%f\n", sampTime,getMotorVelocity(),currentRef, getState(),voltage);
  /* Increment timer count */
  timeCount++;
  sampTime = timeCount*(1.0/freq);

  /* Stop control loop after 20 sec */
  if (sampTime >= 15.0) {
    currentCtrl_stop();
    // Stop Motors
    motorRight(0.0);
    motorLeft(0.0);
  }
}

void currentCtrl_init(void)
{
  /* Initialise timer for use with current control loop */
  if (!_is_init) {
      /* Create timer for communication to serial port */
      _currentCtrl_timerID = osTimerNew(currentLoop_update, osTimerPeriodic, NULL, &_currentCtrl_timerAttr);
      _is_init = 1;
  }
}
