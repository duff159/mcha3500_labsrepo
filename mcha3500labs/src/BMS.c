// BMS.c
#include <stdint.h>
#include <stdlib.h>
#include <math.h>
#include "stm32f4xx_hal.h"
#include "cmsis_os2.h"
#include "uart.h"
#include "BMS.h"


ADC_HandleTypeDef hadc3;
ADC_ChannelConfTypeDef sConfigADC;

void BMS_init(void)
{
    /* Enable ADC3 clock */
    __HAL_RCC_ADC3_CLK_ENABLE();
    /* Enable GPIOA clock */
    __HAL_RCC_GPIOA_CLK_ENABLE();
    /* Initialise PA1 with:
      - Pin 1
      - Analog Mode
      - No pull
      - High frequency           */
    GPIO_InitTypeDef  GPIO_InitStructure;
    GPIO_InitStructure.Pin = GPIO_PIN_1;
    GPIO_InitStructure.Mode = GPIO_MODE_ANALOG;
    GPIO_InitStructure.Pull = GPIO_NOPULL;
    GPIO_InitStructure.Speed = GPIO_SPEED_FREQ_HIGH;
    HAL_GPIO_Init(GPIOA, &GPIO_InitStructure);

    /* Initialise ADC3 with:
      - Instance ADC 3
      - Div 2 Prescaler
      - 12 bit resolution
      - Data align right
      - Continuous conversion mode disabled
      - Number of conversions = 1    */
    hadc3.Instance = ADC1;
    hadc3.Init.ClockPrescaler = ADC_CLOCK_SYNC_PCLK_DIV2;
    hadc3.Init.DataAlign = ADC_DATAALIGN_RIGHT;  // Align data to right most bit
    hadc3.Init.Resolution = ADC_RESOLUTION_12B;  // 12 bit resolution
    hadc3.Init.ContinuousConvMode = DISABLE;
    hadc3.Init.NbrOfConversion = 1;

    /* Initialise ADC3 */
    HAL_ADC_Init(&hadc3);

    /* Configure ADC channel to:
      - Channel 1
      - Rank 1
      - Sampling time 480 cycles
      - Offset 0                          */
    sConfigADC.Channel = ADC_CHANNEL_1;
    sConfigADC.Rank = 1;
    sConfigADC.SamplingTime = ADC_SAMPLETIME_480CYCLES;
    sConfigADC.Offset = 0;
    HAL_ADC_ConfigChannel(&hadc3, &sConfigADC);



    GPIO_InitStructure.Pin = GPIO_PIN_5;
    GPIO_InitStructure.Mode = GPIO_MODE_OUTPUT_PP;
    GPIO_InitStructure.Pull = GPIO_NOPULL;
    GPIO_InitStructure.Speed = GPIO_SPEED_FREQ_HIGH;
    HAL_GPIO_Init(GPIOB, &GPIO_InitStructure);


}

float BMS_readVoltage(void)
{
    /* Start ADC 3 */
    HAL_ADC_Start(&hadc3);
    /* Poll for conversion. Use timeout 0xFF */
    HAL_ADC_PollForConversion(&hadc3, 0xFF);
    /* Get ADC value */
    float ADC_in = HAL_ADC_GetValue(&hadc3);
    /* Stop ADC */
    HAL_ADC_Stop(&hadc3);
    /* Compute voltage from ADC reading. Hint: 2^12-1 = 4095 */
    float voltage = (3.23*ADC_in)/4095.0;
    /* Return the computed voltage */
    return voltage;
}
