#ifndef BMS_H
#define BMS_H

/* Add function prototypes here */
void BMS_init(void);
float BMS_readVoltage(void);

#endif
