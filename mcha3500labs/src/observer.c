#include <stddef.h>
#include <stdint.h>
#include <stdlib.h>
#include <math.h>
#include "cmsis_os2.h"
#include "uart.h"
#include "stm32f4xx_hal.h"
#include "arm_math.h" /* Include STM32 DSP matrix libraries */
#include "controller.h"
#include "observer.h"
#include "IMU.h"
#include "motor.h"

/* Set up timer to update loop at set sample time */
/* Misc. varables */
static uint16_t timeCount;
static double freq = 100; // 200Hz
static double sampTime = 0;
static uint8_t _is_init = 0;
static uint8_t _is_running = 0;

/* Structures defined for timer */
osTimerId_t _observer_timerID;
osTimerAttr_t _observer_timerAttr = {
    .name = "observerTimer"
};

// Add Observer Output Matrices
// Define (A-LC)
static float obsv_A_LC_f32[3*3] =
{
  //-1.1401,   -0.6601,   -2.1401,
  //-0.2188,    0.8882,   -0.2288,
  // 1.1593,    0.6597,    2.1593,

     0.0252,    0.0223,   -0.9748,
     0.0042,    0.9948,   -0.0058,
    -0.0011,    0.0007,    0.9989,
};

/* Observer Gain, L */
static float obsv_L_f32[3*2] =
{
  //  0.6601,    2.1401,
  //  0.1118,    0.2288,
  // -0.6597,   -1.1593,

  -0.022333032816326,   0.974836245063233,
   0.005218144892188,   0.005806463721335,
  -0.000717662774805,   0.001125736837802,


};

static float obsv_xk_f32[3] =
{
  /* Angle, Velocity, Gyro Bias */
  0.0,
  0.0,
  0.0,
};

static float obsv_yk_f32[2] =
{
  /* Angle and Velocity */
  0.0,
  0.0,
};

static float obsv_xhat_f32[3] =
{
  /* Angle^, Velocity^, Gyro Bias^ */
  0.0,
  0.0,
  0.0,
};

static float obsv_yhat_f32[2] =
{
  /* Angle, Velocity */
  0.0,
  0.0,
};

////// Temporary Matrices
static float obsv_3x1_f32[3] =
{
  0.0,
  0.0,
  0.0,
};

static float obsv_3x1v2_f32[3] =
{
  0.0,
  0.0,
  0.0,
};

static float obsv_2x1_f32[2] =
{
  0.0,
  0.0,
};


/* Define control matrix variables */
// rows, columns, data array
arm_matrix_instance_f32 obsv_A_LC = {3, 3, (float32_t *)obsv_A_LC_f32};
arm_matrix_instance_f32 obsv_L = {3, 2, (float32_t *)obsv_L_f32};
arm_matrix_instance_f32 obsv_xk = {3, 1, (float32_t *)obsv_xk_f32};
arm_matrix_instance_f32 obsv_yk = {2, 1, (float32_t *)obsv_yk_f32};
arm_matrix_instance_f32 obsv_xhat = {3, 1, (float32_t *)obsv_xhat_f32};
arm_matrix_instance_f32 obsv_yhat = {2, 1, (float32_t *)obsv_yhat_f32};
arm_matrix_instance_f32 obsv_3x1 = {3, 1, (float32_t *)obsv_3x1_f32};
arm_matrix_instance_f32 obsv_3x1v2 = {3, 1, (float32_t *)obsv_3x1v2_f32};
arm_matrix_instance_f32 obsv_2x1 = {2, 1, (float32_t *)obsv_2x1_f32};

/* Initiobserver matrix values */
void obsv_init(void)
{
  arm_mat_init_f32(&obsv_A_LC, 3, 3, (float32_t *)obsv_A_LC_f32);
  arm_mat_init_f32(&obsv_L, 3, 2, (float32_t *)obsv_L_f32);
  arm_mat_init_f32(&obsv_xk, 3, 1, (float32_t *)obsv_xk_f32);
  arm_mat_init_f32(&obsv_yk, 2, 1, (float32_t *)obsv_yk_f32);
  arm_mat_init_f32(&obsv_xhat, 3, 1, (float32_t *)obsv_xhat_f32);
  arm_mat_init_f32(&obsv_yhat, 2, 1, (float32_t *)obsv_yhat_f32);
  arm_mat_init_f32(&obsv_3x1, 3, 1, (float32_t *)obsv_3x1_f32);
  arm_mat_init_f32(&obsv_3x1v2, 3, 1, (float32_t *)obsv_3x1v2_f32);
  arm_mat_init_f32(&obsv_2x1, 2, 1, (float32_t *)obsv_2x1_f32);
}


/* Update state vector elements */
void obsv_set_x1h(float x1h) //Angle
{
  // Update state x1h, Angle
  obsv_xhat_f32[0] = x1h;
}

void obsv_set_x2h(float x2h) // Velocity
{
  // Update state x2h, Velocity
  obsv_xhat_f32[1] = x2h;
}

void obsv_set_x3h(float x3h) // Bias
{
  // Update state x2h, Velocity
  obsv_xhat_f32[2] = x3h;
}

void obsv_set_y1h(float y1h) // Estimate Angle
{
  // Update output angle
  obsv_yhat_f32[0] = y1h;
}

void obsv_set_y2h(float y2h) // Estimate Velocity
{
  // Update output velocity
  obsv_yhat_f32[1] = y2h;
}


float obsv_get_y1h(void) // Estimate Angle
{
  // Return estimated angle
  return obsv_yhat_f32[1];
}

float obsv_get_y2h(void) // Estimate Velocity
{
  // Return estimated velocity
  return obsv_yhat_f32[0];
}

void observerEstimate(float angle, float velocity)
{
  /* Set yk[0] = IMU Angle */
  obsv_yk_f32[0] = angle;
  /* Set yk[1] = IMU Velocity */
  obsv_yk_f32[1] = velocity;

  // (A-LC)*xhat
  arm_mat_mult_f32(&obsv_A_LC, &obsv_xhat, &obsv_3x1);
  // L*yk
  arm_mat_mult_f32(&obsv_L, &obsv_yk, &obsv_3x1v2);
  // xhat_k+1 = (A-LC)*xhat_k + L*yk
  arm_mat_add_f32(&obsv_3x1, &obsv_3x1v2, &obsv_xhat);


  /* Update ykhat */
  /* Set y1hat = x1hat */
  obsv_yhat_f32[0] = obsv_xhat_f32[0];
  /* Set y2hat = x2hat */
  obsv_yhat_f32[1] = obsv_xhat_f32[1];
}

/* Update abserver output */
void observerCalc_update(void)
{
  /* Read IMU */
  IMU_read();
  /* Compute IMU Estimates */
  /* Get the imu angle from accelerometer readings */
  double accAngle = get_acc_angle();
  /* Get the imu X gyro reading */
  float gyroVel = get_gyroX();

  /* Update Observer Estimate */
  observerEstimate(accAngle, gyroVel);

  //ctrl_set_x2h(obsv_yhat_f32[0]);
  //ctrl_set_x3h(obsv_yhat_f32[1]);
}

void observer_start(void)
{
  /* Reset the time counter */
  timeCount = 0;
  /* Start data logging timer at 100Hz */
  if(!_is_running && _is_init)
    {
        /* Start the timer to run at 100Hz */
        osTimerStart(_observer_timerID,(1.0/freq)*1000);
        _is_running = 1;
    }
}

void observer_stop(void)
{
    /* Stop current control timer */
    /* Check if timer is running */
    if (osTimerIsRunning(_observer_timerID)){
        _is_running = 1;
    }

    if(_is_running && _is_init)
    {
        /* Stop the timer */
        osTimerStop(_observer_timerID);
        _is_running = 0;
        //printf("Observer Estimation OFF. \n");
    } else {
        printf("Observer estimation is not currently running.\n");
    }
}

static void observer_update(void *arg)
{
  /* Supress compiler warnings for unused arguments */
  UNUSED(arg);

  /* Recalculate IMU Estimates */
  observerCalc_update();

  /* Print Results */
  /* Time, IMU Angle, Angle Estimate, IMU Velocity, IMU Estimate */
  printf("%f,%f,%f,%f,%f\n",sampTime,get_acc_angle(),obsv_get_y1h(),get_gyroX(),obsv_get_y2h());


  /* Increment timer count */
  timeCount++;
  sampTime = timeCount*(1.0/freq);

  /* Stop control loop after 20 sec */
  if (sampTime >= 15.0) {
    /* Stop observer loop */
    observer_stop();
  }
}

void observer_timer_init(void)
{
  /* Initialise timer for use with observer loop */
  if (!_is_init) {
      /* Create timer for communication to serial port */
      _observer_timerID = osTimerNew(observer_update, osTimerPeriodic, NULL, &_observer_timerAttr);
      _is_init = 1;
  }
}
