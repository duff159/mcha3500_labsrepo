#ifndef VELOCITYCONTROL_H
#define VELOCITYCONTROL_H

/* Add function prototypes here */
void setVelLeft_refInput(float velocity);
void setVelRight_refInput(float velocity);
void setLeftVelRef(float velocity);
void setRightVelRef(float velocity);
void velocityLeftCtrl_update(void);
void velocityRightCtrl_update(void);
float getLVelControl(void);
float getRVelControl(void);
void velocityCtrl_start(void);
void velocityCtrl_stop(void);
void velocityCtrl_init(void);
float getRVelocity(void);

/*void setIState(float I);
float getState(void);
void setIRef(float current);
float getIControl(void);
void currentCtrl_update(void);
void currentCtrl_init(void);
void currentCtrl_start(void);
void currentCtrl_stop(void);*/

#endif
