#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <string.h>
#include <inttypes.h> // For PRIxx and SCNxx macros
#include "cmsis_os2.h"
#include "stm32f4xx_hal.h" // to import UNUSED() macro
#include "cmd_line_buffer.h"
#include "cmd_parser.h"

/* Structures defined for timers */
osTimerId_t _logging_timerID;
osTimerId_t _currentCtrl_timerID;
osTimerId_t _velocityCtrl_timerID;
osTimerId_t _observer_timerID;
osTimerId_t _controlLoop_timerID;

// Type for each command table entry
typedef struct
{
    void (*func)(int argc, char *argv[]);   // Command function pointer
    const char * cmd;                       // Command name
    const char * args;                      // Command arguments syntax
    const char * help;                      // Command description
} CMD_T;

// Forward declaration for built-in commands
static void _help(int, char *[]);
static void _reset(int, char *[]);
static void _cmd_getPotentiometerVoltage(int, char *[]);
static void _cmd_pendulumLogging(int, char *[]);
static void _cmd_IMULogging(int argc, char *argv[]);
static void _cmd_encoderLogging(int argc, char *argv[]);
static void _cmd_currentLogging(int argc, char *argv[]);
static void _cmd_getControl(int argc, char *argv[]);
static void _cmd_setControlState(int argc, char *argv[]);
static void _cmd_getControlState(int argc, char *argv[]);
static void _cmd_battVoltage(int argc, char *argv[]);
static void _cmd_motorVoltage(int argc, char *argv[]);
static void _cmd_motorLeftVoltage(int argc, char *argv[]);
static void _cmd_motorRightVoltage(int argc, char *argv[]);
static void _cmd_setReference(int argc, char *argv[]);
static void _cmd_sinMotorVoltage(int argc, char *argv[]);
static void _cmd_currentControl(int argc, char *argv[]);
static void _cmd_setIReference(int argc, char *argv[]);
static void _cmd_setIState(int argc, char *argv[]);
static void _cmd_getIControl(int argc, char *argv[]);
static void _cmd_motorVelocity(int argc, char *argv[]);
static void _cmd_velocityControl(int argc, char *argv[]);
static void _cmd_setLVelRef(int argc, char *argv[]);
static void _cmd_setRVelRef(int argc, char *argv[]);
static void _cmd_swingTest(int argc, char *argv[]);
static void _cmd_observerTest(int argc, char *argv[]);
static void _cmd_controlTest(int argc, char *argv[]);
static void _cmd_setSpin(int argc, char *argv[]);
static void _cmd_setLeftTurn(int argc, char *argv[]);
static void _cmd_setRightTurn(int argc, char *argv[]);
// Modules that provide commands
#include "heartbeat_cmd.h"
#include "pendulum.h"
#include "motor.h"
#include "data_logging.h"
#include "controller.h"
#include "BMS.h"
#include "currentSense.h"
#include "currentControl.h"
#include "velocityControl.h"
#include "observer.h"
#include "controlLoop.h"

// Command table
static CMD_T cmd_table[] =
{
    {_help                       , "help"         , ""                          , "Displays this help message"             } ,
    {_reset                      , "reset"        , ""                          , "Restarts the system."                   } ,
    {_cmd_battVoltage            , "getBatt"      , ""                          , "Get the current voltage/charge of the battery."},
    {heartbeat_cmd               , "heartbeat"    , "[start|stop]"              , "Get status or start/stop heartbeat task"} ,
    {_cmd_getPotentiometerVoltage, "getPot"       , ""                          , "Displays the potentiometer voltage level."},
    {_cmd_pendulumLogging        , "logPot"       , "[start|stop]"              , "Start or Stop pendulum data logging at 100Hz for 2 seconds."},
    {_cmd_IMULogging             , "logIMU"       , "[start|stop]"              , "Start or Stop IMU data logging at 100Hz for 5 seconds."},
    {_cmd_encoderLogging         , "logEnc"       , "[start|stop]"              , "Start or Stop encoder data logging at 100Hz for 15 seconds."},
    {_cmd_currentLogging         , "logCurrent"   , "[start|stop]"              , "Start or stop current logging at 100Hz for 10 sec. [left|right]."},
    {_cmd_getControl             , "getControl"   , ""                          , "Get the value of the control signal."},
    {_cmd_setControlState        , "setControl"   , "[state,value]"             , "Set new values of the state variables."},
    {_cmd_getControlState        , "getState"     , "[state]"                   , "Get the value of the state variables."},
    {_cmd_motorVoltage           , "setMotors"    , "<voltage>"                 , "Apply a desired voltage to both motors."},
    {_cmd_motorLeftVoltage       , "setMotorLeft" , "<Voltage>"                 , "Apply a desired voltage to the left motor."   },
    {_cmd_motorRightVoltage      , "setMotorRight", "<Voltage>"                 , "Apply a desired voltage to the right motor."   },
    {_cmd_setReference           , "setYr"        , "<yr>"                      , "Set reference value of wheel velocity."},
    {_cmd_sinMotorVoltage        , "sinVoltage"   , "[start|stop]"              , "Send a sinusoid voltage to the motors."},
    {_cmd_currentControl         , "IControl"     , "[start|stop]"              , "Start a current control loop of the right motor."},
    {_cmd_setIReference          , "setIRef"      , "<I*>"                      , "Set new value of the current reference [I*]."},
    {_cmd_setIState              , "setIState"    , "<I>"                       , "Set new value of the state variable [I]."},
    {_cmd_getIControl            , "getIControl"  , ""                          , "Get the value of the current control signal."},
    {_cmd_motorVelocity          , "getVelocity"  , ""                          , "Get the velocity of the right motor."},
    {_cmd_velocityControl        , "velControl"   , "[start|stop]"              , "Start a velocity control loop of the right motor."},
    {_cmd_setLVelRef             , "setLVelRef"   , "<Velocity>"                , "Set velocity reference of left motor."},
    {_cmd_setRVelRef             , "setRVelRef"   , "<Velocity>"                , "Set velocity reference of right motor."},
    {_cmd_swingTest              , "swingTest"    , "[start|stop]"              , "Start swing test for calibration."},
    {_cmd_observerTest           , "observerTest" , "[start|stop]"              , "Start observer est. of IMU angle and velocity."},
    {_cmd_controlTest            , "controlTest"  , "[start|stop]"              , "Start balancing control loop."},
    {_cmd_setSpin                , "spin"         , "<Spin Velocity>"           , "Set rad/s of robot spinning."},
    {_cmd_setLeftTurn            , "LTurn"        , "<Turn Velocty>"            , "Set rad/s of left wheel turning."},
    {_cmd_setRightTurn           , "RTurn"        , "<Turn Velocty>"            , "Set rad/s of right wheel turning."},
};
enum {CMD_TABLE_SIZE = sizeof(cmd_table)/sizeof(CMD_T)};
enum {CMD_MAX_TOKENS = 5};      // Maximum number of tokens to process (command + arguments)

// Command function definitions

static void _print_chip_pinout(void);

void _help(int argc, char *argv[])
{
    UNUSED(argv);
    printf(
        "\n"
        "\n"
    );

    _print_chip_pinout();

    printf("\n");

    // Describe argument syntax using POSIX.1-2008 convention
    // see http://pubs.opengroup.org/onlinepubs/9699919799/basedefs/V1_chap12.html
    switch (argc)
    {
    case 1:
        printf(
            "   Command Arguments            Description\n"
            "-------------------------------------------\n"
        );
        for (int i = 0; i < CMD_TABLE_SIZE; i++)
        {
            printf("%10s %-20s %s\n", cmd_table[i].cmd, cmd_table[i].args, cmd_table[i].help);
        }
        // printf("\nFor more information, enter help followed by the command name\n\n");
        break;
    case 2:
        printf("Not yet implemented.\n\n");
        // TODO: Scan command table, and lookup extended help string.
        break;
    default:
        printf("help is expecting zero or one argument.\n\n");
    }
}

void _reset(int argc, char *argv[])
{
    UNUSED(argc);
    UNUSED(argv);
    // Reset the system
    HAL_NVIC_SystemReset();
}

void _print_chip_pinout(void)
{
    printf(
        "Pin configuration:\n"
        "\n"
        "       .---------------------------------------.\n"
        " PC10--|  1  2 --PC11              PC9--  1  2 |--PC8\n"
        " PC12--|  3  4 --PD2               PB8--  3  4 |--PC6\n"
        "  VDD--|  5  6 --E5V               PB9--  5  6 |--PC5\n"
        "BOOT0--|  7  8 --GND              AVDD--  7  8 |--U5V\n"
        "   NC--|  9 10 --NC                GND--  9 10 |--NC\n"
        "   NC--| 11 12 --IOREF             PA5-- 11 12 |--PA12\n"
        " PA13--| 13 14 --RESET             PA6-- 13 14 |--PA11\n"
        " PA14--| 15 16 --+3v3              PA7-- 15 16 |--PB12\n"
        " PA15--| 17 18 --+5v               PB6-- 17 18 |--NC\n"
        "  GND--| 19 20 --GND               PC7-- 19 20 |--GND\n"
        "  PB7--| 21 22 --GND               PA9-- 21 22 |--PB2\n"
        " PC13--| 23 24 --VIN               PA8-- 23 24 |--PB1\n"
        " PC14--| 25 26 --NC               PB10-- 25 26 |--PB15\n"
        " PC15--| 27 28 --PA0               PB4-- 27 28 |--PB14\n"
        "  PH0--| 29 30 --PA1               PB5-- 29 30 |--PB13\n"
        "  PH1--| 31 32 --PA4               PB3-- 31 32 |--AGND\n"
        " VBAT--| 33 34 --PB0              PA10-- 33 34 |--PC4\n"
        "  PC2--| 35 36 --PC1               PA2-- 35 36 |--NC\n"
        "  PC3--| 37 38 --PC0               PA3-- 37 38 |--NC\n"
        "       |________                   ____________|\n"
        "                \\_________________/\n"
    );
}

static void _cmd_battVoltage(int argc, char *argv[])
{
  /* Supress compiler warnings for unused arguments */
  UNUSED(argc);
  UNUSED(argv);

  /* Get battery voltage */
  float BMS_voltage = BMS_readVoltage();
  float battVoltage = BMS_voltage*2.6+0.7;
  float battCharge = (battVoltage/8.4)*100;
  /* Print voltage/charge */
  printf("---------- BATTERY STATUS ----------\n");
  printf("Voltage: %.2f V\nCharge [percent]: %.2f \n", battVoltage, battCharge);
  if (battVoltage <= 7.3) {
    printf("CHARGE BATTERY NOW!\n");
  }
}

void _cmd_getPotentiometerVoltage(int argc, char *argv[])
{
  /* Supress compiler warnings for unused arguments */
  UNUSED(argc);
  UNUSED(argv);
  /* Read the potentiometer voltage */
  float voltage = pendulum_read_voltage();
  /* Print the voltage to the serial terminal */
  printf("The potentiometer voltage is: %f\n", voltage);
}
void _cmd_pendulumLogging(int argc, char *argv[])
{
  if (argc <= 1)
  {
    if (osTimerIsRunning(_logging_timerID)) {
      printf("Data is currently being logged\n");
    } else {
      printf("Data is not currently being logged\n");
    }
  }
  else
  {
      if (strcmp(argv[1], "start") == 0)
      {
          logging_start();
      }
      else if (strcmp(argv[1], "stop") == 0)
      {
          logging_stop();
      }
      else
      {
          printf("%s: invalid argument \"%s\", syntax is: %s [start|stop]\n", argv[0], argv[1], argv[0]);
      }
  }
}

void _cmd_IMULogging(int argc, char *argv[])
{
  if (argc <= 1)
  {
    if (osTimerIsRunning(_logging_timerID)) {
      printf("Data is currently being logged\n");
    } else {
      printf("Data is not currently being logged\n");
    }
  }
  else
  {
      if (strcmp(argv[1], "start") == 0)
      {
          imu_logging_start();
      }
      else if (strcmp(argv[1], "stop") == 0)
      {
          logging_stop();
      }
      else
      {
          printf("%s: invalid argument \"%s\", syntax is: %s [start|stop]\n", argv[0], argv[1], argv[0]);
      }
  }
}

void _cmd_encoderLogging(int argc, char *argv[])
{
  if (argc <= 1)
  {
    if (osTimerIsRunning(_logging_timerID)) {
      printf("Data is currently being logged\n");
    } else {
      printf("Data is not currently being logged\n");
    }
  }
  else
  {
      if (strcmp(argv[1], "start") == 0)
      {
          encoder_logging_start();
      }
      else if (strcmp(argv[1], "stop") == 0)
      {
          logging_stop();
      }
      else
      {
          printf("%s: invalid argument \"%s\", syntax is: %s [start|stop]\n", argv[0], argv[1], argv[0]);
      }
  }
}

void _cmd_currentLogging(int argc, char *argv[])
{
  if (argc <= 1)
  {
    if (osTimerIsRunning(_logging_timerID)) {
      printf("Data is currently being logged\n");
    } else {
      printf("Data is not currently being logged\n");
    }
  }
  else
  {
      if (strcmp(argv[1], "start") == 0)
      {
          current_logging_start();
      }
      else if (strcmp(argv[1], "stop") == 0)
      {
          logging_stop();
      }
      else
      {
          printf("%s: invalid argument \"%s\", syntax is: %s [start|stop]\n", argv[0], argv[1], argv[0]);
      }
  }
}

static void _cmd_getControl(int argc, char *argv[]) {
  /* Supress compiler warnings for unused arguments */
  UNUSED(argc);
  UNUSED(argv);

  /* Update controller */
  ctrl_update();
  /* Print control action */
  //printf("%f\n", getControl());
  printf("%f\n", ctrl_get_u());
}

static void _cmd_setControlState(int argc, char *argv[])
{
  // Check for correct input arguments
  if(argc != 3)
  {
    printf("Incorrect arguments\n");
  } else {
    switch(atoi(argv[1])) {
      // Set controller state
      case 1:
        /* Update x1h */
        ctrl_set_x1h(atof(argv[2]));
        break;

      case 2:
        /* Update x2h */
        ctrl_set_x2h(atof(argv[2]));
        break;
      case 3:
        /* Update x3h */
        ctrl_set_x3h(atof(argv[2]));
        break;
      case 4:
        /* Update x4h */
        ctrl_set_x4h(atof(argv[2]));
        break;
      }
    }
}

static void _cmd_getControlState(int argc, char *argv[])
{
  // Check for correct input arguments
  if(argc != 2)
  {
    printf("Incorrect arguments\n");
  } else {
    switch(atoi(argv[1])) {
      // Set controller state
      case 1:
        /* Print x1h */
        printf("%f\n",ctrl_get_x1h());
        break;

      case 2:
        /* Print x2h */
        printf("%f\n",ctrl_get_x2h());
        break;
      case 3:
        /* Print x3h */
        printf("%f\n",ctrl_get_x3h());
        break;
      case 4:
        /* Print x1h */
        printf("%f\n",ctrl_get_x4h());
        break;
      }
    }
}



static void _cmd_motorVoltage(int argc, char *argv[])
{
  // Check for correct input arguments
  if(argc != 2)
  {
    printf("Incorrect arguments\n");
  } else {
    // Set motor voltage
    motorLeft(atof(argv[1]));
    motorRight(atof(argv[1]));
  }

}
static void _cmd_motorLeftVoltage(int argc, char *argv[])
{
  // Check for correct input arguments
  if(argc != 2)
  {
    printf("Incorrect arguments\n");
  } else {
    // Set left motor voltage
    motorLeft(atof(argv[1]));
  }
}

static void _cmd_motorRightVoltage(int argc, char *argv[])
{
  // Check for correct input arguments
  if(argc != 2)
  {
    printf("Incorrect arguments\n");
  } else {
    // Set right motor voltage
    motorRight(atof(argv[1]));
  }
}

static void _cmd_setReference(int argc, char *argv[])
{
  // Check for correct input arguments
  if(argc != 2)
  {
    printf("Incorrect arguments\n");
  } else {
    // Set velocity reference value
    ctrl_set_yr(atof(argv[1]));
    printf("Wheel velocity reference set to %f rad/s.\n", atof(argv[1]));
  }
}

static void _cmd_sinMotorVoltage(int argc, char *argv[])
{
    if (argc <= 1)
    {
      if (osTimerIsRunning(_logging_timerID)) {
        printf("Data is currently being logged\n");
      } else {
        printf("Data is not currently being logged\n");
      }
    }
    else
    {
        if (strcmp(argv[1], "start") == 0)
        {
          // Call sinusoid voltage function
          sinVoltage_logging_start();
        }
        else if (strcmp(argv[1], "stop") == 0)
        {
          logging_stop();
        }
        else
        {
            printf("%s: invalid argument \"%s\", syntax is: %s [start|stop]\n", argv[0], argv[1], argv[0]);
        }
    }
}

/*----------------CURRENT CONTROL-----------------------*/
static void _cmd_currentControl(int argc, char *argv[])
{
  if (argc <= 1) {
    if (osTimerIsRunning(_currentCtrl_timerID)) {
      printf("Current control is not running.\n");
    } else {
      printf("Current control is running.\n");
    }
  }
  else
  {
      if (strcmp(argv[1], "start") == 0)
      {
        // Call current control
        currentCtrl_start();
      }
      else if (strcmp(argv[1], "stop") == 0)
      {
        currentCtrl_stop();
      }
      else
      {
          printf("%s: invalid argument \"%s\", syntax is: %s [start|stop]\n", argv[0], argv[1], argv[0]);
      }
  }
}

static void _cmd_setIReference(int argc, char *argv[])
{
  // Check for correct input arguments
  if(argc != 2) {
    printf("Incorrect arguments\n");
  } else {
    // Set I*
    setIRef(atof(argv[1]));
  }
}
static void _cmd_setIState(int argc, char *argv[])
{
  // Check for correct input arguments
  if(argc != 2)
  {
    printf("Incorrect arguments\n");
  } else {
    // Set state variable (current)
    setIState(atof(argv[1]));
  }
}
static void _cmd_getIControl(int argc, char *argv[])
{
  /* Supress compiler warnings for unused arguments */
  UNUSED(argc);
  UNUSED(argv);

  /* Update current controller */
  currentCtrl_update();
  /* Print current control action */
  printf("%f\n", getIControl());
}
/*^^^^^^^^^^^^^^CURRENT CONTROL^^^^^^^^^^^^^^*/

static void _cmd_motorVelocity(int argc, char *argv[])
{
  /* Supress compiler warnings for unused arguments */
  UNUSED(argc);
  UNUSED(argv);

  /* Update velocity */
  motorVelocity_logging_start();
}

/*----------VELOCITY CONTROL----------*/
static void _cmd_velocityControl(int argc, char *argv[])
{
  if (argc <= 1) {
    if (osTimerIsRunning(_velocityCtrl_timerID)) {
      printf("Velocity control is not running.\n");
    } else {
      printf("Velocity control is running.\n");
    }
  }
  else
  {
      if (strcmp(argv[1], "start") == 0)
      {
        // Start velocity control timer
        velocityCtrl_start();
      }
      else if (strcmp(argv[1], "stop") == 0)
      {
        velocityCtrl_stop();
      }
      else
      {
          printf("%s: invalid argument \"%s\", syntax is: %s [start|stop]\n", argv[0], argv[1], argv[0]);
      }
  }
}
static void _cmd_setLVelRef(int argc, char *argv[])
{
  // Check for correct input arguments
  if(argc != 2)
  {
    printf("Incorrect arguments\n");
  } else {
    // Set left motor velocity ref
    setVelLeft_refInput(atof(argv[1]));
  }
}
static void _cmd_setRVelRef(int argc, char *argv[])
{
  // Check for correct input arguments
  if(argc != 2)
  {
    printf("Incorrect arguments\n");
  } else {
    // Set right motor velocity ref
    setVelRight_refInput(atof(argv[1]));
  }
}
/*^^^^^^^^^^VELOCITY CONTROL^^^^^^^^^^*/
static void _cmd_swingTest(int argc, char *argv[])
{
  if (argc <= 1)
  {
    if (osTimerIsRunning(_logging_timerID)) {
      printf("Data is currently being logged\n");
    } else {
      printf("Data is not currently being logged\n");
    }
  }
  else
  {
      if (strcmp(argv[1], "start") == 0)
      {
        // Call sinusoid voltage function
        swingTest_logging_start();
      }
      else if (strcmp(argv[1], "stop") == 0)
      {
        logging_stop();
      }
      else
      {
          printf("%s: invalid argument \"%s\", syntax is: %s [start|stop]\n", argv[0], argv[1], argv[0]);
      }
  }

}

static void _cmd_observerTest(int argc, char *argv[])
{
  if (argc <= 1)
  {
    if (osTimerIsRunning(_observer_timerID)) {
      printf("Observer is running.\n");
    } else {
      printf("Observer is not running.\n");
    }
  }
  else
  {
      if (strcmp(argv[1], "start") == 0)
      {
        // Start observer loop
        observer_start();
      }
      else if (strcmp(argv[1], "stop") == 0)
      {
        observer_stop();
      }
      else
      {
          printf("%s: invalid argument \"%s\", syntax is: %s [start|stop]\n", argv[0], argv[1], argv[0]);
      }
  }
}

static void _cmd_controlTest(int argc, char *argv[])
{
  if (argc <= 1)
  {
    if (osTimerIsRunning(_controlLoop_timerID)) {
      printf("Balancing control is running.\n");
    } else {
      printf("Balancing control is not running.\n");
    }
  }
  else
  {
      if (strcmp(argv[1], "start") == 0)
      {
        // Start velocity control timer
        //velocityCtrl_start();
        // Start balancing loop
        controlLoop_start();
      }
      else if (strcmp(argv[1], "stop") == 0)
      {
        // Stop balancing loop
        controlLoop_stop();
        // Stop velocity control loop
        velocityCtrl_stop();
      }
      else
      {
          printf("%s: invalid argument \"%s\", syntax is: %s [start|stop]\n", argv[0], argv[1], argv[0]);
      }
  }
}

static void _cmd_setSpin(int argc, char *argv[])
{
  // Check for correct input arguments
  if(argc != 2)
  {
    printf("Incorrect arguments\n");
  } else {
    // Set spin velocity ref
    spin(atof(argv[1]));
  }
}

static void _cmd_setLeftTurn(int argc, char *argv[])
{
  // Check for correct input arguments
  if(argc != 2)
  {
    printf("Incorrect arguments\n");
  } else {
    // Set left wheel turn velocity
    setLeftTurn(atof(argv[1]));
  }
}

static void _cmd_setRightTurn(int argc, char *argv[])
{
  // Check for correct input arguments
  if(argc != 2)
  {
    printf("Incorrect arguments\n");
  } else {
    // Set right wheel turn velocity
    setRightTurn(atof(argv[1]));
  }
}

// Command parser and dispatcher

static int _makeargv(char *s, char *argv[], int argvsize);

#ifdef NO_LD_WRAP
void cmd_parse(char *) __asm__("___real_cmd_parse");
#endif

void cmd_parse(char * cmd)
{
    if (cmd == NULL)
    {
        printf("ERROR: Tried to parse NULL command pointer\n");
        return;
    }
    else if (*cmd == '\0') // Empty command string
    {
        return;
    }

    // Tokenise command string
    char *argv[CMD_MAX_TOKENS];
    int argc = _makeargv(cmd, argv, CMD_MAX_TOKENS);

    // Execute corresponding command function
    for (int i = 0; i < CMD_TABLE_SIZE; i++)
    {
        if (strcmp(argv[0], cmd_table[i].cmd) == 0)
        {
            cmd_table[i].func(argc, argv);
            return;
        }
    }

    // Command not found
    printf("Unknown command: \"%s\"\n", argv[0]);
}

// Command tokeniser

int _makeargv(char *s, char *argv[], int argvsize)
{
    char *p = s;
    int argc = 0;

    for(int i = 0; i < argvsize; ++i)
    {
        // skip leading whitespace
        while (isspace(*p))
            p++;

        if(*p != '\0')
            argv[argc++] = p;
        else
        {
            argv[argc] = NULL;
            break;
        }

        // scan over arg
        while(*p != '\0' && !isspace(*p))
            p++;

        // terminate arg
        if(*p != '\0' && i < argvsize - 1)
            *p++ = '\0';
    }

    return argc;
}
