#ifndef CONTROLLER_H
#define CONTROLLER_H

enum {
  CTRL_N_INPUT = 1, // number of controller inputs (reference signals)
  CTRL_N_STATE = 4, // number of controller states (states)
  CTRL_N_OUTPUT = 1, // number of controller outputs / plant inputs
};

void ctrl_set_xStar(void);
void ctrl_set_uStar(void);
void ctrl_init(void);
void ctrl_set_x1h(float x1h);
float ctrl_get_x1h(void);
void ctrl_set_x2h(float x2h);
float ctrl_get_x2h(void);
void ctrl_set_x3h(float x3h);
float ctrl_get_x3h(void);
void ctrl_set_x4h(float x4h);
float ctrl_get_x4h(void);
void ctrl_set_yr(float yr);
float ctrl_get_u(void);
float getControl(void);
void ctrl_update(void);

#endif
