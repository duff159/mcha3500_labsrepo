#ifndef CURRENTCONTROL_H
#define CURRENTCONTROL_H

/* Add function prototypes here */
void setIState(float I);
float getState(void);
void setIRef(float current);
float getIControl(void);
void currentCtrl_update(void);
void currentCtrl_init(void);
void currentCtrl_start(void);
void currentCtrl_stop(void);

#endif
