#include <stdint.h>

#include "stm32f4xx_hal.h"
#include "cmsis_os2.h"
#include "uart.h"
#include "pendulum.h"
#include "math.h"
#include "data_logging.h"
#include "IMU.h"
#include "tm_stm32_mpu6050.h"

#ifndef M_PI
#define M_PI 3.14159265358979323846
#endif

/* Variable declarations */
TM_MPU6050_t IMU_datastruct;
/* Function defintions */
void IMU_init(void)
{
  /* Initialise IMU with AD0 LOW, accelleration sensitivity +-4g, gyroscope +-250 deg/s */
  TM_MPU6050_Init(&IMU_datastruct, TM_MPU6050_Device_0, TM_MPU6050_Accelerometer_4G, TM_MPU6050_Gyroscope_250s);
}

void IMU_read(void)
{
  /* Read all IMU values */
  TM_MPU6050_ReadAll(&IMU_datastruct);
}

float get_accY(void)
{
  /* Convert accelleration reading to ms^-2 */
  int16_t accelY = IMU_datastruct.Accelerometer_Y;
  float result_accY = ((accelY*4)/32767.0)*9.81;

  /* return the Y acceleration */
  return result_accY;
}

float get_accZ(void)
{
  /* Convert acceleration reading to ms^-2 */
  int16_t accelZ = IMU_datastruct.Accelerometer_Z;
  float result_accZ = ((accelZ*4)/32767.0)*9.81;

  /* return the Z acceleration */
  return result_accZ;
}

float get_gyroX(void)
{
  /* return the X angular velocity */
  int16_t gyroX = IMU_datastruct.Gyroscope_X;

  float result_gyroX = (-((gyroX*250)/32767.0)*(M_PI/180)); // minus the mean

  /* return the X angular velocity */
  return result_gyroX;
}

double get_acc_angle(void)
{
  /* compute IMU angle using accY and accZ using atan2 */
  double angle = (atan2(get_accZ(), get_accY()));;//-0.0761;


  /* return the IMU angle */
  return angle;
}
