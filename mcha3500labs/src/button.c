// button.c
#include "button.h"
#include "stm32f4xx_hal.h"

static uint8_t _was_pressed = 0;    // Sticky flag
static uint8_t _is_init = 0;

void button_init(void)
{
    _was_pressed = 0;

    if (!_is_init)  // Guards against multiple initialisations
    {
        /*    Enable GPIOC clock                          */
        /*          Initialise PC13 with
                    - External interrupts on the rising edge
                    - Pulldown resistors enabled                */
        __HAL_RCC_GPIOC_CLK_ENABLE();

        /* Define structure to initialise GPIO */
        /* Fill structure with correct parameters for
                    pin, mode, pull and speed */
        GPIO_InitTypeDef  GPIO_InitStructure;
        GPIO_InitStructure.Pin = GPIO_PIN_13;
        GPIO_InitStructure.Mode = GPIO_MODE_IT_RISING_FALLING;
        GPIO_InitStructure.Pull = GPIO_PULLUP;
        GPIO_InitStructure.Speed = GPIO_SPEED_FREQ_HIGH;
        /*    Initialise GPIO using HAL function */
        HAL_GPIO_Init(GPIOC, &GPIO_InitStructure);

        /* Configure priority of interrupt */
        //HAL_NVIC_SetPriority(EXTI15_10_IRQn, 0x0f, 0x0f);
        /*     Enable the EXTI15_10 interrupt   */
        HAL_NVIC_EnableIRQ(EXTI15_10_IRQn);

        _is_init = 1;
    }
}

void button_deinit(void)
{
    _is_init = 0;
    _was_pressed = 0;
}

void button_clear_pressed(void)
{
    _was_pressed = 0;
}

uint8_t button_get_pressed(void)
{
    return _was_pressed;
}

void button_set_pressed(void)
{
    _was_pressed = 1;
}

uint8_t button_pop_pressed(void)
{
    // Atomically read _was_pressed
    uint8_t was_pressed = _was_pressed;
    _was_pressed = 0;
    return was_pressed;
}

void button_isr(void)
{
    _was_pressed = 1;
}

void EXTI15_10_IRQHandler(void)
{
    /*  Call the button isr */
    button_isr();
    //button_set_pressed();
    /* Call the HAL GPIO EXTI IRQ Handler and specify the GPIO pin */
    HAL_GPIO_EXTI_IRQHandler(GPIO_PIN_13);
}
