#ifndef DATA_LOGGING_H
#define DATA_LOGGING_H


/* Add function prototypes here */
void logging_init(void);
void logging_start(void);
void logging_stop(void);
void imu_logging_start(void);
void encoder_logging_start(void);
void current_logging_start(void);
void sinVoltage_logging_start(void);
void motorVelocity_logging_start(void);
float getMotorRVelocity(void);
float getMotorRVelocity(void);
void currentCalibrate_start(void);
float getCurrent1_zero(void);
float getCurrent2_zero(void);
void swingTest_logging_start(void);
#endif
