#ifndef MOTOR_H
#define MOTOR_H


/* Add function prototypes here */
void motor_PWM_init(void);
void motor_encoder_init(void);
void EXTI0_IRQHandler(void);
int32_t motor_encoder_getValue(void);
int32_t motor_encoder2_getValue(void);
void motorSetSpeed(int motorNum, int DIR, int SPEED);
void motorRight(float voltage);
void motorLeft(float voltage);

#endif
