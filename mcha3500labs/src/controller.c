#include <stddef.h>
#include "stm32f4xx_hal.h"
#include "arm_math.h" /* Include STM32 DSP matrix libraries */
#include "controller.h"

#ifndef M_PI
#define M_PI 3.14159265358979323846
#endif
/* Define control matrix values */
static float ctrl_mK_f32[CTRL_N_INPUT*CTRL_N_STATE] =
{
  /* Cart-Pole System */
  /* negative K, 1x4 */
  //43.9434, 9.1494, 6.8254, 0.9429,

  /* Balancing Robot Original Sim */
  /* negative K, 1x4 */
  //13.9852, 2.5697, 6.7965, 0.9416,


  /* REAL Balancing Robot - Velocity Control (Slew Rate) IA */
  511.922832, 214.207198, -96.691714, 1.598992,
  //514.015847, 209.371804, -96.711649, 1.592128, // E Best so far

   /* PIL Test */
   //39.4227,   32.1517,   -5.4041,    0.9733,

};
static float ctrl_xHat_f32[CTRL_N_STATE] =
{
  /* estimate of state, 4x1 */
  0.0,  // Momentum
  0.0,  // Angle
  0.0,  // Omega
  0.0,  // Integrator
};
static float ctrl_u_f32[CTRL_N_INPUT] =
{
  /* control action, 1x1 */
  0.0, // Acceleration
};

static float ctrl_yr_f32[CTRL_N_OUTPUT] =
{
  /* Reference value yr = u* */
  0.0,
};

/* Define reference feedforward gains */
static float ctrl_Nx_f32[CTRL_N_STATE-1] =
{
  /* Feedforward x gain, Nx */
  0.207256320,
  1.850130493e-16,
  1.0,
};
static float ctrl_Nu_f32[CTRL_N_INPUT] =
{
  /* Feedforward u gain, Nu */
  0.0,
};

static float ctrl_xStar_f32[CTRL_N_STATE-1] =
{
  /* x* */
  0.0,
  0.0,
  0.0,

};
static float ctrl_uStar_f32[CTRL_N_INPUT] =
{
  /* u* */
  0.0,
};

/* Define control matrix variables */
// rows, columns, data array
arm_matrix_instance_f32 ctrl_mK = {CTRL_N_INPUT, CTRL_N_STATE, (float32_t *)ctrl_mK_f32};
arm_matrix_instance_f32 ctrl_xHat = {CTRL_N_STATE, 1, (float32_t *)ctrl_xHat_f32};
arm_matrix_instance_f32 ctrl_u = {CTRL_N_INPUT, 1, (float32_t *)ctrl_u_f32};
arm_matrix_instance_f32 ctrl_yr = {CTRL_N_OUTPUT, 1, (float32_t *)ctrl_yr_f32};
arm_matrix_instance_f32 ctrl_Nx = {CTRL_N_STATE, 1, (float32_t *)ctrl_Nx_f32};
arm_matrix_instance_f32 ctrl_Nu = {CTRL_N_INPUT, 1, (float32_t *)ctrl_Nu_f32};
arm_matrix_instance_f32 ctrl_xStar = {CTRL_N_STATE, 1, (float32_t *)ctrl_xStar_f32};
arm_matrix_instance_f32 ctrl_uStar = {CTRL_N_INPUT, 1, (float32_t *)ctrl_uStar_f32};

/* Control functions */
void ctrl_init(void)
{
  arm_mat_init_f32(&ctrl_mK, CTRL_N_INPUT, CTRL_N_STATE, (float32_t *)ctrl_mK_f32);
  arm_mat_init_f32(&ctrl_xHat, CTRL_N_STATE, 1, (float32_t *)ctrl_xHat_f32);
  arm_mat_init_f32(&ctrl_u, CTRL_N_INPUT, 1, (float32_t *)ctrl_u_f32);
  arm_mat_init_f32(&ctrl_yr, CTRL_N_OUTPUT, 1, (float32_t *)ctrl_yr_f32);
  arm_mat_init_f32(&ctrl_Nx, CTRL_N_STATE, 1, (float32_t *)ctrl_Nx_f32);
  arm_mat_init_f32(&ctrl_Nu, CTRL_N_INPUT, 1, (float32_t *)ctrl_Nu_f32);
  arm_mat_init_f32(&ctrl_xStar, CTRL_N_STATE, 1, (float32_t *)ctrl_xStar_f32);
  arm_mat_init_f32(&ctrl_uStar, CTRL_N_INPUT, 1, (float32_t *)ctrl_uStar_f32);

  /* Set reference values */
  ctrl_set_xStar();
  ctrl_set_uStar();
}

void ctrl_set_yr(float yr)
{
  ctrl_yr_f32[0] = yr;
  /* Update xStar and uStar values */
  ctrl_set_xStar();
  ctrl_set_uStar();
}

void ctrl_set_xStar(void)
{
  // Compute xStar
  arm_mat_mult_f32(&ctrl_Nx, &ctrl_yr, &ctrl_xStar);
}

void ctrl_set_uStar(void)
{
  // Compute uStar
  arm_mat_mult_f32(&ctrl_Nu, &ctrl_yr, &ctrl_uStar);
}

/* Update state vector elements */
void ctrl_set_x1h(float x1h)
{
  // Update state x1h
  ctrl_xHat_f32[0] = x1h - ctrl_xStar_f32[0];
}

float ctrl_get_x1h(void)
{
  // Return state x1h
  return ctrl_xHat_f32[0];
}

void ctrl_set_x2h(float x2h)
{
  // Update state x2h
  ctrl_xHat_f32[1] = x2h -  ctrl_xStar_f32[1];
}

float ctrl_get_x2h(void)
{
  // Return state x2h
  return ctrl_xHat_f32[1];
}

void ctrl_set_x3h(float x3h)
{
  // Update state x3h
  ctrl_xHat_f32[2] = x3h - ctrl_xStar_f32[2];
}

float ctrl_get_x3h(void)
{
  // Return state x3h
  return ctrl_xHat_f32[2];
}

void ctrl_set_x4h(float x4h)
{
  // Update state x4h
  ctrl_xHat_f32[3] = x4h;// - ctrl_xStar_f32[3];
}

float ctrl_get_x4h(void)
{
  // Return state x4h
  return ctrl_xHat_f32[3];
}

float ctrl_get_u(void)
{
  return ctrl_u_f32[0];
}

/* Get the current control output (wheel velocity) */
float getControl(void)
{
  /* Slew Rate Velocity Control */

  float ctrlVal = ctrl_xHat_f32[2];  // omega

  if (ctrl_xHat_f32[2] > 20.0) {
    ctrlVal = 20.0;

  } else if (ctrl_xHat_f32[2] < -20.0) {
    ctrlVal = -20.0;
  }

  return ctrlVal;
}

/* Update control output */
void ctrl_update(void)
{
  /* Velocity Control (Slew Rate) */
  arm_mat_mult_f32(&ctrl_mK, &ctrl_xHat, &ctrl_u);

  ctrl_xHat_f32[2] = (ctrl_xHat_f32[2] - ctrl_xStar_f32[2]) + 0.01 * ctrl_u_f32[0]; // Ts = 0.01

  ctrl_xHat_f32[3] = ctrl_xHat_f32[3] + 0.01 * ctrl_xHat_f32[2]; // Ts = 0.01
}
