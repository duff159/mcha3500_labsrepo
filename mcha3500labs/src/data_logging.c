#include <stdint.h>
#include <stdlib.h>
#include <math.h>
#include "stm32f4xx_hal.h"
#include "cmsis_os2.h"
#include "uart.h"
#include "pendulum.h"
#include "motor.h"
#include "IMU.h"
#include "currentSense.h"
#include "data_logging.h"


/* Variable declarations */
uint16_t logCount;
double frequency = 100; // 100Hz
double sampleTime = 0;
int32_t encLCount1 =  0;
int32_t encRCountOld =  0;
int32_t encLCount2 =  0;
int32_t encRCountNew =  0;
float velocityRight = 0;
float velocityLeft = 0;
float zeroVal_1 = 2.51;
float zeroVal_2 = 2.51;
float sum1 = 0.0;
float sum2 = 0.0;
static void (*log_function)(void);
/* Misc. varables */
static uint8_t _is_init = 0;
static uint8_t _is_running = 0;


/* Structures defined for data logging */
osTimerId_t _logging_timerID;
osTimerAttr_t _logging_timerAttr = {
    .name = "loggingTimer"
};

/* Function declarations */
static void log_pointer(void *argument);
static void log_pendulum(void);
static void log_imu(void);
static void log_encoder(void);
static void log_current(void);
static void log_motorSinVoltage(void);
static void motorVelocity(void);
static void currentSense_calibrate(void);
static void log_swingTest(void);

/* Function defintions */
void logging_init(void)
{
  /* Initialise timer for use with pendulum data logging */
  if (!_is_init) {
      /* Create timer for communication to serial port */
      _logging_timerID = osTimerNew(log_pointer, osTimerPeriodic, NULL, &_logging_timerAttr);
      _is_init = 1;
  }
}
static void log_pointer(void *argument)
{
  /* Supress compiler warnings for unused arguments */
  UNUSED(argument);

  /* Call function pointed to by log_function */
  (*log_function)();
}

static void log_pendulum(void)
{
  /* Read the potentiometer voltage */
  float voltage = pendulum_read_voltage();

  /* Print the sample time and potentiometer voltage to the serial terminal in the format [time],[voltage] */
  printf("%f,%f\n", sampleTime, voltage);

  /* Increment log count */
  logCount++;
  sampleTime = logCount*(1.0/frequency);

  /* Stop logging once 2 seconds is reached (Complete this once you have created the stop functionin the next step) */
  if (sampleTime >= 2.0) {
    logging_stop();
  }
}

void logging_start(void)
{
  /* Change function pointer to the pendulum logging function */
  log_function = &log_pendulum;
  /* Reset the log counter */
  logCount = 0;
  /* Start data logging timer at 100Hz */
  if(!_is_running && _is_init)
    {
        /* Start the timer to run at 100Hz */
        osTimerStart(_logging_timerID,(1.0/frequency)*1000);
        _is_running = 1;
    }
}

void logging_stop(void)
{
  /* Stop data logging timer */
  /* Check if timer is running  */
    if (osTimerIsRunning(_logging_timerID)){
        _is_running = 1;
    }

    if(_is_running && _is_init)
    {
        /* Stop the timer */
        osTimerStop(_logging_timerID);
        _is_running = 0;
        //printf("Data Logging off \n");
    } else {
        printf("Data Logging is not currently running \n");
    }

}
void imu_logging_start(void)
{
  /* Change function pointer to the imu logging function (log_imu) */
  log_function = &log_imu;
  /* Reset the log counter */
  logCount = 0;

  /* Start data logging timer at 100Hz */
  if(!_is_running && _is_init)
    {
        /* Start the timer to run at 100Hz */
        osTimerStart(_logging_timerID,(1.0/200)*1000);
        _is_running = 1;
    }
}

static void log_imu(void)
{
  /* Read IMU */
  IMU_read();
  /* Get the imu angle from accelerometer readings */
  double accAngle = get_acc_angle();
  /* Get the imu X gyro reading */
  float gyro_X = get_gyroX();
  /* Read the encoder count */
  int32_t encCount = motor_encoder2_getValue();
  /* Print the time, accelerometer angle, gyro angular velocity and encoder count values to the
  serial terminal in the format %f,%f,%f,%f\n */
  printf("%f,%f,%f,%li\n", sampleTime, accAngle, gyro_X, encCount);
  /* Increment log count */
  logCount++;
  sampleTime = logCount*(1.0/200);
  /* Stop logging once 10 seconds is reached */
  if (sampleTime >= 10.0) {
    logging_stop();
  }
}


void encoder_logging_start(void)
{
  /* Change function pointer to the encoder logging function */
  log_function = &log_encoder;
  /* Reset the log counter */
  logCount = 0;

  /* Start data logging timer at 100Hz */
  if(!_is_running && _is_init)
    {
        /* Start the timer to run at 100Hz */
        osTimerStart(_logging_timerID,(1.0/frequency)*1000);
        _is_running = 1;
    }
}

static void log_encoder(void)
{
  int32_t encLCount = motor_encoder_getValue();
  int32_t encRCount = -motor_encoder2_getValue();

  /* Print the time and encoder values to the
  serial terminal in the format %f,%li,%li\n */

  printf("%f,%li,%li\n", sampleTime, encLCount, encRCount);
  /* Increment log count */
  logCount++;
  sampleTime = logCount*(1.0/frequency);
  /* Stop logging once 10 seconds is reached */
  if (sampleTime >= 10.0) {
    logging_stop();
  }
}

void motorVelocity_logging_start(void) {
  /* Change function pointer to the velocity logging function */
  log_function = &motorVelocity;
  /* Reset the log counter */
  logCount = 0;

  /* Start data logging timer at 100Hz */
  if(!_is_running && _is_init)
    {
        /* Start the timer to run at 100Hz */
        osTimerStart(_logging_timerID,(1.0/frequency)*1000);
        _is_running = 1;
    }
}

static void motorVelocity(void) {

  encRCountNew = motor_encoder2_getValue();
  velocityRight = -((((encRCountNew-encRCountOld)/(48.0*34.014))*360.0)/0.01)*(3.14159/180);
  encRCountOld = encRCountNew;

  /* Print data */
  printf("%li,%li,%f\n", encRCountOld, encRCountNew, velocityRight);

  logCount++;
  sampleTime = logCount*(1.0/frequency);
  /* Stop logging once 5 seconds is reached */
  if (sampleTime >= 5.0) {

    logging_stop();
    logCount = 0;
    sampleTime = 0;
    encRCountNew = 0.0;
    encRCountOld = 0.0;
  }
}
float getMotorRVelocity(void) {
  return velocityRight;
}
float getMotorLVelocity(void) {
  return velocityLeft;
}

void current_logging_start(void)
{
  /* Change function pointer to the current logging function */
  log_function = &log_current;
  /* Reset the log counter */
  logCount = 0;

  /* Start data logging timer at 100Hz */
  if(!_is_running && _is_init)
    {
        /* Start the timer to run at 100Hz */
        osTimerStart(_logging_timerID,(1.0/frequency)*1000);
        _is_running = 1;
    }
}

static void log_current(void)
{
  currentSense_read();
  float currentLeft = getCurrent1();
  float currentRight = getCurrent2();
  /* Print the time and current values to the
  serial terminal in the format %f,%f,%f\n */
  printf("%f,%f,%f\n", sampleTime, currentLeft, currentRight);


  /* Increment log count */
  logCount++;
  sampleTime = logCount*(1.0/frequency);
  /* Stop logging once 10 seconds is reached */
  if (sampleTime >= 30.0) {
    logging_stop();
    sampleTime = 0;
  }
}

void sinVoltage_logging_start(void)
{
  /* Change function pointer to the logging function */
  log_function = &log_motorSinVoltage;
  /* Reset the log counter */
  logCount = 0;

  /* Start data logging timer at 100Hz */
  if(!_is_running && _is_init)
    {
        /* Start the timer to run at 100Hz */
        osTimerStart(_logging_timerID,(1.0/frequency)*1000);
        _is_running = 1;
    }
}

static void log_motorSinVoltage(void)
{
  /* Compute sinusoid voltage */
  float voltage = 4.5*sin((1.5)*sampleTime);
  motorRight(voltage);
  motorLeft(voltage);

  // Get the encoder counts of the motors
  int32_t encLeftCount = motor_encoder_getValue();
  int32_t encRightCount = -motor_encoder2_getValue();

  // Get the current value
  //currentSense_read();
  //float currentRight = getCurrent1();
  //float currentLeft = -getCurrent2();

  /* Print results (time, encoder left, encoder right, voltage) */
  printf("%f,%li,%li,%f\n", sampleTime,encLeftCount,encRightCount,voltage);
  /* Increment log count */
  logCount++;
  sampleTime = logCount*(1.0/frequency);
  /* Stop logging once 30 seconds is reached */
  if (sampleTime >= 10.0) {
    logging_stop();
    // Stop the motors
    motorRight(0.0);
    motorLeft(0.0);
    sampleTime = 0;
  }
}

void currentCalibrate_start(void)
{
  /* Change function pointer to the calibrate function */
  log_function = &currentSense_calibrate;
  /* Reset the log counter */
  logCount = 0;

  /* Start data logging timer at 100Hz */
  if(!_is_running && _is_init)
    {
        /* Start the timer to run at 100Hz */
        osTimerStart(_logging_timerID,(1.0/frequency)*1000);
        _is_running = 1;
    }
}

static void currentSense_calibrate(void)
{
  /* Read current sensors */
  currentSense_read();
  /* Get the current of the right motor */
  float current1 = getCurrent1();
  /* Get the current of the left motor */
  float current2 = getCurrent2();

  /* Sum the current "zero" values */
  sum1 = sum1 + current1;
  sum2 = sum2 + current2;

  /* Calculate the average zero value of the sensors */
  zeroVal_1 = sum1/300;
  zeroVal_2 = sum2/300;


  /* Increment log count */
  logCount++;
  sampleTime = logCount*(1.0/frequency);
  /* Stop calibration once 2 seconds reached */
  if (sampleTime >= 2.0) {
    logging_stop();
    sampleTime = 0;
    printf("Current Sensor Calibration Complete!\nRight Sensor Zero Value: %f\nLeft Sensor Zero Value: %f\n", zeroVal_1,zeroVal_2);
  }
}

float getCurrent1_zero(void)
{
  return zeroVal_1;
}

float getCurrent2_zero(void)
{
  return zeroVal_2;
}

void swingTest_logging_start(void)
{
  /* Change function pointer to the swing test logging function */
  log_function = &log_swingTest;
  /* Reset the log counter */
  logCount = 0;

  /* Start data logging timer at 100Hz */
  if(!_is_running && _is_init)
    {
        /* Start the timer to run at 100Hz */
        osTimerStart(_logging_timerID,(1.0/frequency)*1000);
        _is_running = 1;
    }

}
static void log_swingTest(void)
{
  /* Read IMU */
  IMU_read();
  /* Get the imu angle from accelerometer readings */
  double accAngle = get_acc_angle();
  /* Get the imu X gyro reading */
  float gyro_X = get_gyroX();
  /* Read the encoder count */
  int32_t encCount = motor_encoder2_getValue(); // pos because using swing test rig encoder
  /* Print the time, accelerometer angle, gyro angular velocity and encoder count values to the
  serial terminal in the format %f,%f,%f,%f\n */
  printf("%f,%f,%f,%li\n", sampleTime, accAngle, gyro_X, encCount);
  /* Increment log count */
  logCount++;
  sampleTime = logCount*(1.0/frequency);
  /* Stop logging once 10 seconds is reached */
  if (sampleTime >= 10.0) {
    logging_stop();
  }
}
