#ifndef CURRENTSENSE_H
#define CURRENTSENSE_H


/* Add function prototypes here */
void currentSense_init(void);
void currentSense_read(void);
float getCurrent1(void);
float getCurrent2(void);
#endif
