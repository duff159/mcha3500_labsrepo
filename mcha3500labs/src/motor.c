// Motor.c
#include <stdint.h>
#include <stdlib.h>
#include <math.h>
#include "stm32f4xx_hal.h"
#include "cmsis_os2.h"
#include "uart.h"
#include "motor.h"
#include "BMS.h"

#define FREQUENCY 3000

int32_t enc_count = 0;
int32_t enc2_count = 0;
static TIM_HandleTypeDef htim3;
static TIM_HandleTypeDef htim4;
static TIM_HandleTypeDef htim8;
int32_t PWMperiod;
void motor_PWM_init(void)
{
    /* Enable TIM3 clock */
    __HAL_RCC_TIM3_CLK_ENABLE();
    /* Enable TIM4 clock */
    __HAL_RCC_TIM4_CLK_ENABLE();
    /* Enable TIM8 clock */
    __HAL_RCC_TIM8_CLK_ENABLE();

    /* Enable GPIOA clock */
    __HAL_RCC_GPIOA_CLK_ENABLE();
    /* Enable GPIOB clock */
    __HAL_RCC_GPIOB_CLK_ENABLE();
    /* Enable GPIOC clock */
    __HAL_RCC_GPIOC_CLK_ENABLE();

/* Compute period */
int freqSTM = 1e8;
int freqPWM = FREQUENCY;
float T_STM = 1/(float)freqSTM;
float T_PWM = 1/(float)freqPWM;
PWMperiod =  (int32_t)(T_PWM/T_STM);
//printf("%li", PWMperiod);

//-------------MOTOR 1-------------------------
/* --------MOTOR 1 SPEED - AIN1---------- */
/* Initialise PC7 with:
  - Pin 7
  - Alternate function push-pull mode
  - No pull
  - High frequency
  - Alternate function 2 - Timer 8*/
GPIO_InitTypeDef GPIO_InitStruct;
GPIO_InitStruct.Pin = GPIO_PIN_7;
GPIO_InitStruct.Mode = GPIO_MODE_AF_PP;
GPIO_InitStruct.Pull = GPIO_NOPULL;
GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_HIGH;
GPIO_InitStruct.Alternate = GPIO_AF3_TIM8;
/* Call the HAL function to initialise GPIOA pin 7 */
HAL_GPIO_Init(GPIOC, &GPIO_InitStruct);

/* Initialise timer 8 with:
  - Instance TIM8
  - Prescaler of 0
  - Counter mode up
  - Timer period to generate a 10kHz signal
  - Clock division of 1 */
//TIM_HandleTypeDef htim8;
htim8.Instance = TIM8;
htim8.Init.Prescaler = 1;
htim8.Init.CounterMode = TIM_COUNTERMODE_UP;
htim8.Init.Period = PWMperiod;
htim8.Init.ClockDivision = 0;
HAL_TIM_PWM_Init(&htim8);

/* Configure timer 8, channel 2 with:
  - Output compare mode PWM1
  - Pulse = 0
  - OC polarity high
  - Fast mode disabled */
TIM_OC_InitTypeDef sConfig;
sConfig.OCMode = TIM_OCMODE_PWM1;
sConfig.Pulse = 0;
sConfig.OCPolarity = TIM_OCPOLARITY_HIGH;
sConfig.OCFastMode = TIM_OCFAST_DISABLE;
HAL_TIM_PWM_ConfigChannel(&htim8, &sConfig, TIM_CHANNEL_2);

/* Set initial Timer 8, channel 2 compare value */
__HAL_TIM_SET_COMPARE(&htim8, TIM_CHANNEL_2, 0);
/* Start timer 8 channel 2 */
HAL_TIM_PWM_Start(&htim8, TIM_CHANNEL_2);

/* --------MOTOR 1 DIRECTION - AIN2---------- */
/* Initialise PB6 with:
  - Pin 6
  - Alternate function push-pull mode
  - No pull
  - High frequency
  - Alternate function 2 - Timer 4*/
//GPIO_InitTypeDef GPIO_InitStruct;
GPIO_InitStruct.Pin = GPIO_PIN_6;
GPIO_InitStruct.Mode = GPIO_MODE_AF_PP;
GPIO_InitStruct.Pull = GPIO_NOPULL;
GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_HIGH;
GPIO_InitStruct.Alternate = GPIO_AF2_TIM4;
/* Call the HAL function to initialise GPIOA pin 6 */
HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);

/* Initialise timer 4 with:
  - Instance TIM4
  - Prescaler of 0
  - Counter mode up
  - Timer period to generate a 10kHz signal
  - Clock division of 1 */
//TIM_HandleTypeDef htim4;
htim4.Instance = TIM4;
htim4.Init.Prescaler = 1;
htim4.Init.CounterMode = TIM_COUNTERMODE_UP;
htim4.Init.Period = PWMperiod;
htim4.Init.ClockDivision = 0;
HAL_TIM_PWM_Init(&htim4);

/* Configure timer 4, channel 1 with:
  - Output compare mode PWM1
  - Pulse = 0
  - OC polarity high
  - Fast mode disabled */
//static TIM_OC_InitTypeDef sConfig;
sConfig.OCMode = TIM_OCMODE_PWM1;
sConfig.Pulse = 0;
sConfig.OCPolarity = TIM_OCPOLARITY_HIGH;
sConfig.OCFastMode = TIM_OCFAST_DISABLE;
HAL_TIM_PWM_ConfigChannel(&htim4, &sConfig, TIM_CHANNEL_1);

/* Set initial Timer 4, channel 1 compare value */
__HAL_TIM_SET_COMPARE(&htim4, TIM_CHANNEL_1, 0);
/* Start timer 4 channel 1 */
HAL_TIM_PWM_Start(&htim4, TIM_CHANNEL_1);


//-------------MOTOR 2--------------------------
    /* --------MOTOR 2 SPEED - AIN1---------- */
    /* Initialise PA6 with:
      - Pin 6
      - Alternate function push-pull mode
      - No pull
      - High frequency
      - Alternate function 2 - Timer 3*/
    //GPIO_InitTypeDef GPIO_InitStruct;
    GPIO_InitStruct.Pin = GPIO_PIN_6;
    GPIO_InitStruct.Mode = GPIO_MODE_AF_PP;
    GPIO_InitStruct.Pull = GPIO_NOPULL;
    GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_HIGH;
    GPIO_InitStruct.Alternate = GPIO_AF2_TIM3;
    /* Call the HAL function to initialise GPIOA pin 6 */
    HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);

    /* Initialise timer 3 with:
      - Instance TIM3
      - Prescaler of 0
      - Counter mode up
      - Timer period to generate a 50kHz signal
      - Clock division of 1 */
    //TIM_HandleTypeDef htim3;
    htim3.Instance = TIM3;
    htim3.Init.Prescaler = 1;
    htim3.Init.CounterMode = TIM_COUNTERMODE_UP;
    htim3.Init.Period = PWMperiod;
    htim3.Init.ClockDivision = 0;
    HAL_TIM_PWM_Init(&htim3);

    /* Configure timer 3, channel 1 with:
      - Output compare mode PWM1
      - Pulse = 0
      - OC polarity high
      - Fast mode disabled */
    //static TIM_OC_InitTypeDef sConfig;
    sConfig.OCMode = TIM_OCMODE_PWM1;
    sConfig.Pulse = 0;
    sConfig.OCPolarity = TIM_OCPOLARITY_HIGH;
    sConfig.OCFastMode = TIM_OCFAST_DISABLE;
    HAL_TIM_PWM_ConfigChannel(&htim3, &sConfig, TIM_CHANNEL_1);

    /* Set initial Timer 3, channel 1 compare value */
    __HAL_TIM_SET_COMPARE(&htim3, TIM_CHANNEL_1, 0);
    /* Start timer 3 channel 1 */
    HAL_TIM_PWM_Start(&htim3, TIM_CHANNEL_1);

    /* --------MOTOR 2 DIRECTION - AIN2---------- */
    /* Initialise PA7 with:
      - Pin 7
      - Alternate function push-pull mode
      - No pull
      - High frequency
      - Alternate function 2 - Timer 3*/
    //GPIO_InitTypeDef GPIO_InitStruct;
    GPIO_InitStruct.Pin = GPIO_PIN_7;
    GPIO_InitStruct.Mode = GPIO_MODE_AF_PP;
    GPIO_InitStruct.Pull = GPIO_NOPULL;
    GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_HIGH;
    GPIO_InitStruct.Alternate = GPIO_AF2_TIM3;
    /* Call the HAL function to initialise GPIOA pin 6 */
    HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);

    /* Initialise timer 3 with:
      - Instance TIM3
      - Prescaler of 0
      - Counter mode up
      - Timer period to generate a 10kHz signal
      - Clock division of 1 */
    //static TIM_HandleTypeDef htim3;
    htim3.Instance = TIM3;
    htim3.Init.Prescaler = 1;
    htim3.Init.CounterMode = TIM_COUNTERMODE_UP;
    htim3.Init.Period = PWMperiod;
    htim3.Init.ClockDivision = 0;
    HAL_TIM_PWM_Init(&htim3);

    /* Configure timer 3, channel 1 with:
      - Output compare mode PWM1
      - Pulse = 0
      - OC polarity high
      - Fast mode disabled */
    //static TIM_OC_InitTypeDef sConfig;
    sConfig.OCMode = TIM_OCMODE_PWM1;
    sConfig.Pulse = 0;
    sConfig.OCPolarity = TIM_OCPOLARITY_HIGH;
    sConfig.OCFastMode = TIM_OCFAST_DISABLE;
    HAL_TIM_PWM_ConfigChannel(&htim3, &sConfig, TIM_CHANNEL_2);

    /* Set initial Timer 3, channel 2 compare value */
    __HAL_TIM_SET_COMPARE(&htim3, TIM_CHANNEL_2, 0);
    /* Start timer 3 channel 2 */
    HAL_TIM_PWM_Start(&htim3, TIM_CHANNEL_2);


}

void motorSetSpeed(int motorNum, int DIR, int dutyCycle) {

  if (motorNum == 1) {
    // Set motor 1 DIR
    if (DIR == 0) {
      // Backwards
      __HAL_TIM_SET_COMPARE(&htim4, TIM_CHANNEL_1, 0);
      // Set Motor 1 SPEED
      __HAL_TIM_SET_COMPARE(&htim8, TIM_CHANNEL_2, PWMperiod*(dutyCycle/100.0));
      printf("Motor 1 spinning backwards at %i percent duty cycle.\n",dutyCycle);
    } else {
      // Forwards
      __HAL_TIM_SET_COMPARE(&htim8, TIM_CHANNEL_2, 0);
      // Set Motor 1 SPEED
      __HAL_TIM_SET_COMPARE(&htim4, TIM_CHANNEL_1,PWMperiod*(dutyCycle/100.0));
      printf("Motor 1 spinning forwards at %i percent duty cycle.\n",dutyCycle);
    }

  } else if (motorNum == 2) {
    // Set motor 2
    // Set motor 2 DIR
    if (DIR == 0) {
      // Backwards
      __HAL_TIM_SET_COMPARE(&htim3, TIM_CHANNEL_2, 0);
      // Set Motor 2 SPEED
      __HAL_TIM_SET_COMPARE(&htim3, TIM_CHANNEL_1, PWMperiod*(dutyCycle/100.0));
      printf("Motor 2 spinning backwards at %i percent duty cycle.\n",dutyCycle);
    } else {
      // Forwards
      __HAL_TIM_SET_COMPARE(&htim3, TIM_CHANNEL_1, 0);
      // Set Motor 2 SPEED
      __HAL_TIM_SET_COMPARE(&htim3, TIM_CHANNEL_2, PWMperiod*(dutyCycle/100.0));
      printf("Motor 2 spinning forwards at %i percent duty cycle.\n",dutyCycle);
    }

  } else {
    // Set both motors
    // Set motor DIR
    if (DIR == 0) {
      /* Motor 1 */
      // Backwards
      __HAL_TIM_SET_COMPARE(&htim4, TIM_CHANNEL_1, 0);
      // Set Motor 1 SPEED
      __HAL_TIM_SET_COMPARE(&htim8, TIM_CHANNEL_2, PWMperiod*(dutyCycle/100.0));
      printf("Motor 1 spinning backwards at %i percent duty cycle.\n",dutyCycle);

      /* Motor 2 */
      // Backwards
      __HAL_TIM_SET_COMPARE(&htim3, TIM_CHANNEL_2, 0);
      // Set Motor 2 SPEED
      __HAL_TIM_SET_COMPARE(&htim3, TIM_CHANNEL_1, PWMperiod*(dutyCycle/100.0));
      printf("Motor 2 spinning backwards at %i percent duty cycle.\n",dutyCycle);
    } else {
      // Forwards
      /* Motor 1 */
      __HAL_TIM_SET_COMPARE(&htim8, TIM_CHANNEL_2, 0);
      // Set Motor 1 SPEED
      __HAL_TIM_SET_COMPARE(&htim4, TIM_CHANNEL_1,PWMperiod*(dutyCycle/100.0));
      printf("Motor 1 spinning forwards at %i percent duty cycle.\n",dutyCycle);

      /* Motor 2 */
      __HAL_TIM_SET_COMPARE(&htim3, TIM_CHANNEL_1, 0);
      // Set Motor 2 SPEED
      __HAL_TIM_SET_COMPARE(&htim3, TIM_CHANNEL_2, PWMperiod*(dutyCycle/100.0));
      printf("Motor 2 spinning forwards at %i percent duty cycle.\n",dutyCycle);
    }
  }
}

/* Apply a voltge input to the left motor (Motor 1) */
/* Max voltage 6.5V (80% Duty Cycle) */
void motorLeft(float voltage)
{
  /* Compute battery voltage */
  float maxVoltage = BMS_readVoltage()*2.6;

  if (voltage > maxVoltage) {
    printf("Warning! Voltage exceeds motor rated voltage (NOT APPLIED)\n");
  } else {
    if (voltage >= 0) {
      // Spin Forwards
      // Compute duty cycle
      float dutyCycle = voltage/maxVoltage;
      __HAL_TIM_SET_COMPARE(&htim8, TIM_CHANNEL_2, 0);
      // Set Motor 1 SPEED
      __HAL_TIM_SET_COMPARE(&htim4, TIM_CHANNEL_1,PWMperiod*dutyCycle);
    //  printf("Left Motor Forwards Duty Cycle: %f.\n",dutyCycle*100);

    } else {
      // Spin Backwards
      // Compute duty cycle
      float dutyCycle = -voltage/maxVoltage;
      __HAL_TIM_SET_COMPARE(&htim4, TIM_CHANNEL_1, 0);
      // Set Motor 1 SPEED
      __HAL_TIM_SET_COMPARE(&htim8, TIM_CHANNEL_2,PWMperiod*dutyCycle);
    //  printf("Left Motor Backwards Duty Cycle: %f.\n",dutyCycle*100);
    }
  }
}

/* Apply a voltge input to the right motor (Motor 2) */
/* Max voltage 6.5V (80% Duty Cycle) */
void motorRight(float voltage)
{
  /* Compute battery voltage */
  float maxVoltage = BMS_readVoltage()*2.6;

  if (voltage > maxVoltage) {
    printf("Warning! Voltage exceeds motor rated voltage (NOT APPLIED)\n");
  } else {
    if (voltage >= 0) {
      // Spin Forwards
      // Compute duty cycle
      float dutyCycle = voltage/maxVoltage;
      __HAL_TIM_SET_COMPARE(&htim3, TIM_CHANNEL_1, 0);
      // Set Motor 2 SPEED
      __HAL_TIM_SET_COMPARE(&htim3, TIM_CHANNEL_2, PWMperiod*dutyCycle);
      //printf("Right Motor Forwards Duty Cycle: %f.\n",dutyCycle*100);

    } else {
      // Spin Backwards
      // Compute duty cycle
      float dutyCycle = -voltage/maxVoltage;
      __HAL_TIM_SET_COMPARE(&htim3, TIM_CHANNEL_2, 0);
      // Set Motor 2 SPEED
      __HAL_TIM_SET_COMPARE(&htim3, TIM_CHANNEL_1, PWMperiod*dutyCycle);
      //("Right Motor Backwards Duty Cycle: %f.\n",dutyCycle*100);
    }
  }
}


void motor_encoder_init(void)
{
    /* Enable GPIOC clock */
    __HAL_RCC_GPIOC_CLK_ENABLE();

    /* MOTOR 1 ENCODER */
    /* Initialise PC0 (A), PC1 (B) with:
      - Pin 0|1
      - Interrupt rising and falling edge
      - No pull
      - High frequency */
    GPIO_InitTypeDef  GPIO_InitStructure;
    GPIO_InitStructure.Pin = GPIO_PIN_0|GPIO_PIN_1;
    GPIO_InitStructure.Mode = GPIO_MODE_IT_RISING_FALLING;
    GPIO_InitStructure.Pull = GPIO_NOPULL;
    GPIO_InitStructure.Speed = GPIO_SPEED_FREQ_HIGH;
    HAL_GPIO_Init(GPIOC, &GPIO_InitStructure);

    /* MOTOR 2 ENCODER */
    /* Initialise PC3 (A), PC2 (B) with:
      - Pin 3|2
      - Interrupt rising and falling edge
      - No pull
      - High frequency */
    GPIO_InitStructure.Pin = GPIO_PIN_3|GPIO_PIN_2;
    GPIO_InitStructure.Mode = GPIO_MODE_IT_RISING_FALLING;
    GPIO_InitStructure.Pull = GPIO_NOPULL;
    GPIO_InitStructure.Speed = GPIO_SPEED_FREQ_HIGH;
    HAL_GPIO_Init(GPIOC, &GPIO_InitStructure);



    /* Set priority of external interrupt lines 0,1,2,3 to 0x0f, 0x0f
       To find the IRQn_Type definition see "MCHA3500 Windows Toolchain\workspace\STM32Cube_F4_FW\Drivers\
       CMSIS\Device\ST\STM32F4xx\Include\stm32f446xx.h" */
    HAL_NVIC_SetPriority(EXTI0_IRQn, 0x0f, 0x0f);
    HAL_NVIC_SetPriority(EXTI1_IRQn, 0x0f, 0x0f);
    HAL_NVIC_SetPriority(EXTI3_IRQn, 0x0f, 0x0f);
    HAL_NVIC_SetPriority(EXTI2_IRQn, 0x0f, 0x0f);

    /* Enable external interrupt for lines 0, 1, 3, 2 */
    HAL_NVIC_EnableIRQ(EXTI0_IRQn);
    HAL_NVIC_EnableIRQ(EXTI1_IRQn);
    HAL_NVIC_EnableIRQ(EXTI3_IRQn);
    HAL_NVIC_EnableIRQ(EXTI2_IRQn);
}
/*  Encoder 1 Pin A */
void EXTI0_IRQHandler(void)
{
    /* Check if PC0 == PC1. Adjust encoder count accordingly. */
    if (HAL_GPIO_ReadPin(GPIOC, GPIO_PIN_0) == HAL_GPIO_ReadPin(GPIOC, GPIO_PIN_1)) {
      enc_count++;
    } else {
      enc_count--;
    }

    /* Reset interrupt */
    HAL_GPIO_EXTI_IRQHandler(GPIO_PIN_0);
}
/*  Encoder 1 Pin B */
void EXTI1_IRQHandler(void)
{
    /* Check if PC1 == PC0. Adjust encoder count accordingly. */
    if (HAL_GPIO_ReadPin(GPIOC, GPIO_PIN_1) == HAL_GPIO_ReadPin(GPIOC, GPIO_PIN_0)) {
      enc_count--;
    } else {
      enc_count++;
    }
    /* Reset interrupt */
    HAL_GPIO_EXTI_IRQHandler(GPIO_PIN_1);
}
/*  Encoder 2 Pin A */
void EXTI3_IRQHandler(void)
{
    /* Check if PC3 == PC2. Adjust encoder count accordingly. */
    if (HAL_GPIO_ReadPin(GPIOC, GPIO_PIN_3) == HAL_GPIO_ReadPin(GPIOC, GPIO_PIN_2)) {
      enc2_count++;
    } else {
      enc2_count--;
    }

    /* Reset interrupt */
    HAL_GPIO_EXTI_IRQHandler(GPIO_PIN_3);
}
/*  Encoder 2 Pin B */
void EXTI2_IRQHandler(void)
{
    /* Check if PC2 == PC3. Adjust encoder count accordingly. */
    if (HAL_GPIO_ReadPin(GPIOC, GPIO_PIN_2) == HAL_GPIO_ReadPin(GPIOC, GPIO_PIN_3)) {
      enc2_count--;
    } else {
      enc2_count++;
    }

    /* Reset interrupt */
    HAL_GPIO_EXTI_IRQHandler(GPIO_PIN_2);
}

int32_t motor_encoder_getValue(void)
{
    return enc_count;
}

int32_t motor_encoder2_getValue(void)
{
    return enc2_count;
}
