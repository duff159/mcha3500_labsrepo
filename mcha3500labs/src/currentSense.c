// currentSense.c
#include <stdint.h>
#include <stdlib.h>
#include <math.h>
#include "stm32f4xx_hal.h"
#include "cmsis_os2.h"
#include "uart.h"
#include "motor.h"
#include "currentSense.h"
#include "data_logging.h"

ADC_HandleTypeDef hadc2;
ADC_ChannelConfTypeDef sConfigADC;
float voltage1;
float voltage2;
float zeroVal_1;
float zeroVal_2;

void currentSense_init(void)
{
    /* Enable ADC2 clock */
    __HAL_RCC_ADC2_CLK_ENABLE();

    //----------CURRENT SENSE 1----------

    /* Enable GPIOC clock */
    __HAL_RCC_GPIOC_CLK_ENABLE();
    /* Initialise PC4 with:
      - Pin 4
      - Analog Mode
      - No pull
      - High frequency           */
    GPIO_InitTypeDef  GPIO_InitStructure;
    GPIO_InitStructure.Pin = GPIO_PIN_4;
    GPIO_InitStructure.Mode = GPIO_MODE_ANALOG;
    GPIO_InitStructure.Pull = GPIO_NOPULL;
    GPIO_InitStructure.Speed = GPIO_SPEED_FREQ_HIGH;
    HAL_GPIO_Init(GPIOC, &GPIO_InitStructure);

    //----------CURRENT SENSE 2----------
    /* Enable GPIOB clock */
    __HAL_RCC_GPIOB_CLK_ENABLE();
    /* Initialise PB1 with:
      - Pin 1
      - Analog Mode
      - No pull
      - High frequency           */
    GPIO_InitStructure.Pin = GPIO_PIN_1;
    GPIO_InitStructure.Mode = GPIO_MODE_ANALOG;
    GPIO_InitStructure.Pull = GPIO_NOPULL;
    GPIO_InitStructure.Speed = GPIO_SPEED_FREQ_HIGH;
    HAL_GPIO_Init(GPIOB, &GPIO_InitStructure);

    /* Initialise ADC2 with:
      - Instance ADC 2
      - Div 2 Prescaler
      - 12 bit resolution
      - Data align right
      - Continuous conversion mode disabled
      - Number of conversions = 1    */
    hadc2.Instance = ADC2;
    hadc2.Init.ClockPrescaler = ADC_CLOCK_SYNC_PCLK_DIV2;
    hadc2.Init.DataAlign = ADC_DATAALIGN_RIGHT;  // Align data to right most bit
    hadc2.Init.Resolution = ADC_RESOLUTION_12B;  // 12 bit resolution
    hadc2.Init.ScanConvMode = ENABLE;
    hadc2.Init.ContinuousConvMode = ENABLE;
    hadc2.Init.EOCSelection = ADC_EOC_SINGLE_CONV;
    hadc2.Init.NbrOfConversion = 2;              // 2 Current Sensors -> 2 Conversions


    /* Initialise ADC2 */
    HAL_ADC_Init(&hadc2);

    /* Configure ADC2 channel to:
      - Channel 14
      - Rank 1
      - Sampling time 480 cycles
      - Offset 0                          */
    sConfigADC.Channel = ADC_CHANNEL_14;
    sConfigADC.Rank = 1;
    sConfigADC.SamplingTime = ADC_SAMPLETIME_480CYCLES;
    sConfigADC.Offset = 0;
    HAL_ADC_ConfigChannel(&hadc2, &sConfigADC);


    /* Configure ADC2 Channel 9 - Rank 2 */
    sConfigADC.Channel = ADC_CHANNEL_9;
    sConfigADC.Rank = 2;
    HAL_ADC_ConfigChannel(&hadc2, &sConfigADC);




}

void currentSense_read(void)
{
    /* Start ADC 2 */
    HAL_ADC_Start(&hadc2);
    /* ----- Current Sensor 1 ----- */
    /* Poll for conversion. Use timeout 0xFF */
    HAL_ADC_PollForConversion(&hadc2, 0xFF);
    /* Get ADC value Current Sensor 1 */
    uint16_t ADC_in1 = HAL_ADC_GetValue(&hadc2);
    /* Compute voltage from ADC reading. Hint: 2^12-1 = 4095 */
    voltage1 =(3.3*ADC_in1)/4095.0;

    /* ----- Current Sensor 1 ----- */
    /* Poll for conversion. Use timeout 0xFF */
    HAL_ADC_PollForConversion(&hadc2, 0xFF);
    /* Get ADC value Current Sensor 2 */
    uint16_t ADC_in2 = HAL_ADC_GetValue(&hadc2);
    /* Compute voltage from ADC reading. Hint: 2^12-1 = 4095 */
    voltage2 = (3.3*ADC_in2)/4095.0;
    //printf("Current Sense 2 voltage is: %f\n", voltage2);

    /* Stop ADC */
    HAL_ADC_Stop(&hadc2);


}

float getCurrent1(void)
{

  float temp = voltage1 - getCurrent1_zero();
  float current1 = temp/0.185;
  return current1;

}

float getCurrent2(void)
{
  float temp = voltage2 - getCurrent2_zero();
  float current2 = temp/0.185;
  return current2;

}
