// velocityControl.c
#include <stdint.h>
#include <stdlib.h>
#include <math.h>
#include "stm32f4xx_hal.h"
#include "cmsis_os2.h"
#include "uart.h"
#include "motor.h"
#include "currentSense.h"
#include "data_logging.h"
#include "controller.h"

#ifndef M_PI
#define M_PI 3.14159265358979323846
#endif

 /* Set up timer to update loop at set sample time */
 /* Misc. varables */
 static uint16_t timeCount;
 static double freq = 200; // 200Hz
 static double sampTime = 0;
 static uint8_t _is_init = 0;
 static uint8_t _is_running = 0;

 /* Structures defined for dtimer */
 osTimerId_t _velocityCtrl_timerID;
 osTimerAttr_t _velocityCtrl_timerAttr = {
     .name = "velocityCtrlTimer"
 };

/* Define Variables */
static float velocityRefL = 0.0; //  Left Velocity Reference
static float velocityRefR = 0.0; //  Right Velocity Reference
static float z_Left = 0.0;       // Left Motor Integrator state
static float z_Right = 0.0;      // Right Motor Integrator state
static float wStarLeft = 0.0;    // Left Motor Velocity Reference
static float wStarRight = 0.0;   // Right Motor Velocity Reference
static float Kw_L =0.2693; //0.4 // Left Motor constant  //0.0031;
static float Kw_R =0.2839;       // Right Motor constant
static float refL = 0.0;         // Velocity Reference of Left Motor
static float refR = 0.0;         // Velocity Reference of Right Motor
static float u_Left = 0.0;       // Left Motor Control input
static float u_Right = 0.0;      // Right Motor Control input
static float errorL = 0.0;       // I - I* (Left Motor)
static float errorR = 0.0;       // I - I* (Right Motor)
static float velocityL = 0.0;    // Left wheel velocity
static float velocityR = 0.0;    // Right wheel velocity
static float voltageL = 0.0;     // Left Motor voltage
static float voltageR = 0.0;     // Right Motor voltage

/* Left Encoder Counts */
static int32_t encLOld = 0;
static int32_t encLNew = 0;
/* Right Encoder Counts */
static int32_t encROld = 0;
static int32_t encRNew = 0;

/* Define PI Gains */
// Left Motor Gains
static float L_Kp =1.5; //1.8
static float L_Ki =0.0;

// Right Motor Gains
static float R_Kp =1.5; //1.5
static float R_Ki = 0.0;

/* Function defintions */
// Velocity ref from terminal
void setVelLeft_refInput(float velocity)
{
  refL = velocity;
}
/*static float getVelLeft_refInput(void)
{
  return refL;
}*/
// Velocity ref from terminal
void setVelRight_refInput(float velocity)
{
  refR = velocity;
}
/*
static float getVelRight_refInput(void)
{
  return refR;
}*/
// Compute velocity of the left wheel
static void setLVelocity(void)
{
  encLNew = motor_encoder_getValue();
  velocityL = ((encLNew-encLOld)/(48.0*34.014*(1/freq)))*2*M_PI;
  encLOld = encLNew;
}
static float getLVelocity(void)
{
  return velocityL;
}

// Compute velocity of right wheel
static void setRVelocity(void)
{
  encRNew = motor_encoder2_getValue();
  velocityR = -((encRNew-encROld)/(48.0*34.014*(1/freq)))*2*M_PI;
  encROld = encRNew;
}
float getRVelocity(void)
{
  return velocityR;
}

// Velocity ref for control
void setLeftVelRef(float velocity)
{
  wStarLeft = velocity;
}
static float getLVelocityRef(void)
{
  return wStarLeft;
}

// Velocity ref for control
void setRightVelRef(float velocity)
{
  wStarRight = velocity;
}
static float getRVelocityRef(void)
{
  return wStarRight;
}

// Set desired voltage of left motor
static void setLMotorVoltage(float ctrl_in)
{
    /* Vin = Kwwa* + u  ?? */
    voltageL = Kw_L*getLVelocityRef() + ctrl_in; //Ra*getCurrentRef() + Kw*getMotorVelocity() + ctrl_in;

    /* Check if voltage is below motors rating */
    if (voltageL >= 6.0) {
      motorLeft(6.0);
    } else if (voltageL <= -6.0) {
      motorLeft(-6.0);
    } else {
      /* Send voltage to left motor */
      motorLeft(voltageL);
    }
}
// Set desired voltage of right motor
static void setRMotorVoltage(float ctrl_in)
{
    /* Vin = Kw x w* + u  ?? */
    voltageR = Kw_R*getRVelocityRef() + ctrl_in; //Ra*getCurrentRef() + Kw*getMotorVelocity() + ctrl_in;

    /* Check if voltage is below motors rating */
    if (voltageR >= 6.0) {
      motorRight(6.0);
    } else if (voltageR <= -6.0) {
      motorRight(-6.0);
    } else {
      /* Send voltage to right motor */
      motorRight(voltageR);
    }
}

/* Update left motor control output */
void velocityLeftCtrl_update(void)
{
  /* Read left velocity state */
  setLVelocity();
  /* Read current error */
  errorL = (getLVelocity() - getLVelocityRef());
  // Increment integral state zk+1 = zk + T*(w-*w)
  z_Left = z_Left + (1/freq)*errorL;

  // Compute control signal u = -Kp*z_dot -Ki*z
  u_Left = -L_Kp*errorL -L_Ki*z_Left;

  // Compute voltage to send to left motor
  setLMotorVoltage(u_Left);
}
/* Update right motor control output */
void velocityRightCtrl_update(void)
{
  /* Read velocity state */
  setRVelocity();
  /* Read current error */
  errorR = (getRVelocity() - getRVelocityRef());
  // Increment integral state zk+1 = zk + T*(w-*w)
  z_Right = z_Right + (1/freq)*errorR;

  // Compute control signal u = -Kp*z_dot -Ki*z
  u_Right = -R_Kp*errorR -R_Ki*z_Right;

  // Compute voltage to send to right motor
  setRMotorVoltage(u_Right);
}

float getLVelControl(void)
{
  velocityLeftCtrl_update();
  return u_Left;
}

float getRVelControl(void)
{
  velocityRightCtrl_update();
  return u_Right;
}

void velocityCtrl_start(void)
{
  /* Reset the time counter */
  timeCount = 0;
  /* Start data logging timer at 100Hz */
  if(!_is_running && _is_init)
    {
        /* Start the timer to run at 100Hz */
        osTimerStart(_velocityCtrl_timerID,(1.0/freq)*1000);
        _is_running = 1;
    }
}

void velocityCtrl_stop(void)
{
    /* Stop velocity control timer */
    /* Check if timer is running */
    if (osTimerIsRunning(_velocityCtrl_timerID)){
        _is_running = 1;
    }

    if(_is_running && _is_init)
    {
        /* Stop the timer */
        osTimerStop(_velocityCtrl_timerID);
        _is_running = 0;
        //printf("Velocity control OFF. \n");
    } else {
        printf("Velocity control is not currently running.\n");
    }
}

static void velocityLoop_update(void *arg)
{
  /* Supress compiler warnings for unused arguments */
  UNUSED(arg);

  /* Update velocity left motor reference */
  //velocityRefL = getVelLeft_refInput();

  /* Test sinusoid velocity reference */

  velocityRefL =15*sin(5*sampTime);
  setLeftVelRef(velocityRefL);


  /* Update velocity right motor reference */
  //velocityRefR = getVelRight_refInput();

  /* Test sinusoid velocity reference */
  velocityRefR =15*sin(5*sampTime);
  setRightVelRef(velocityRefR);

  /* Update the motor velocity reference from the controller */
  //setLeftVelRef(getControl());
  //setRightVelRef(getControl());

  /* Update Control of Both motors */
  velocityLeftCtrl_update();
  velocityRightCtrl_update();

  /* Print Results */
  /* Time, Left Velocity, Left Velocity*, Left Voltage, Right Velocity, Right Velocity*, Right Voltage */
  printf("%f,%f,%ld,%f,%ld, %f\n", sampTime,getLVelocity(),motor_encoder_getValue(), getRVelocity(), -motor_encoder2_getValue(),velocityRefR);
  /* Increment timer count */
  timeCount++;
  sampTime = timeCount*(1.0/freq);

  /* Stop control loop after 20 sec */
  if (sampTime >= 5.0) {
    velocityCtrl_stop();
    // Stop Motors
    motorRight(0.0);
    motorLeft(0.0);
  }
}
void velocityCtrl_init(void)
{
  /* Initialise timer for use with current control loop */
  if (!_is_init) {
      /* Create timer for communication to serial port */
      _velocityCtrl_timerID = osTimerNew(velocityLoop_update, osTimerPeriodic, NULL, &_velocityCtrl_timerAttr);
      _is_init = 1;
  }
}
