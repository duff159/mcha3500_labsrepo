#ifndef CONTROLLOOP_H
#define CONTROLLOOP_H

/* Add function prototypes here */
float getMomentum(void);
void spin(float spinVal);
void setLeftTurn(float turnVal);
void setRightTurn(float turnVal);
void controlLoop_timer_init(void);
void controlLoop_start(void);
void controlLoop_stop(void);


#endif
