#ifndef OBSERVER_H
#define OBSERVER_H

enum {
    OBS_N_INPUT  = 0, // number of observer inputs
    OBS_N_STATE  = 3, // number of observer states
    OBS_N_OUTPUT = 2, // number of plant outputs (angle, velocity, ?)
};
/* Add function prototypes here */
void obsv_init(void);
void obsv_set_x1h(float x1h);
void obsv_set_x2h(float x2h);
void obsv_set_x3h(float x3h);
void obsv_set_y1h(float y1h);
void obsv_set_y2h(float y2h);
float obsv_get_y1h(void);
float obsv_get_y2h(void);
void observer_timer_init(void);
void observerCalc_update(void);
void observer_start(void);
void observer_stop(void);


#endif
