#ifndef TM_DEFINES_H
#define TM_DEFINES_H

/* Put your global defines for all libraries here used in your project */

#define MPU6050_I2C             I2C3
#define MPU6050_I2C_PINSPACK    TM_I2C_PinsPack_1

#endif
